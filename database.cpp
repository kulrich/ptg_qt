#include "database.h"
#include <QString>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>

Database::Database()
{
    DatabaseConnect();
    DatabaseInit();
    DatabasePopulate();
    DatabasePopulate2();

    int fehlercount = 0;
}

void Database::DatabaseConnect()
{
    const QString DRIVER("QSQLITE");

    if(QSqlDatabase::isDriverAvailable(DRIVER))
    {
        QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);

        db.setDatabaseName(":memory:");

        if(!db.open())
            qWarning() << "MainWindow::DatabaseConnect - ERROR: " << db.lastError().text();
    }
    else
        qWarning() << "MainWindow::DatabaseConnect - ERROR: no driver " << DRIVER << " available";
}

void Database::DatabaseInit()
{
    QSqlQuery query("CREATE TABLE pokedex (id INTEGER PRIMARY KEY, name TEXT, gen INTEGER, legi INTEGER, phase INTEGER, type1 TEXT, type2 TEXT)");
    if(!query.isActive())
        qWarning() << "MainWindow::DatabaseInit - ERROR: " << query.lastError().text();
    QSqlQuery queryy("CREATE TABLE pokedex2(id INTEGER PRIMARY KEY ,name TEXT, type_1 TEXT, type_2 TEXT, total INTEGER, hp INTEGER, attack INTEGER, defense INTEGER, sp_Atk INTEGER, sp_Def INTEGER, speed INTEGER, generation INTEGER, legendary TEXT)");
    if(!queryy.isActive())
        qWarning() << "MainWindow::DatabaseInit - ERROR: " << queryy.lastError().text();
}

void Database::DatabasePopulate()
{
    QSqlQuery query;
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(1, 'Bisasam', 1, 0, 0, 'pflanze', 'gift')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(2, 'Bisaknosp', 1, 0, 1, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(3, 'Bisaflor', 1, 0, 2, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(4, 'Glumanda', 1, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(5, 'Glutexo', 1, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(6, 'Glurak', 1, 0, 2, 'feuer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(7, 'Schiggy', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(8, 'Schillok', 1, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(9, 'Turtok', 1, 0, 2, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(10, 'Raupy', 1, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(11, 'Safcon', 1, 0, 1, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(12, 'Smettbo', 1, 0, 2, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(13, 'Hornliu', 1, 0, 0, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(14, 'Kokuna', 1, 0, 1, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(15, 'Bibor', 1, 0, 2, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(16, 'Taubsi', 1, 0, 0, 'normal', 'flug')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(17, 'Tauboga', 1, 0, 1, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(18, 'Tauboss', 1, 0, 2, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(19, 'Rattfratz', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(20, 'Rattikarl', 1, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(21, 'Habitak', 1, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(22, 'Ibitak', 1, 0, 1, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(23, 'Rettan', 1, 0, 0, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(24, 'Arbok', 1, 0, 1, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(25, 'Pikachu', 1, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(26, 'Raichu', 1, 0, 1, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(27, 'Sandan', 1, 0, 0, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(28, 'Sandamer', 1, 0, 1, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(29, 'Nidoran w', 1, 0, 0, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(30, 'Nidorina', 1, 0, 1, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(31, 'Nidoqueen', 1, 0, 2, 'gift', 'boden')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(32, 'Nidoran m', 1, 0, 0, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(33, 'Nidorino', 1, 0, 1, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(34, 'Nidoking', 1, 0, 2, 'gift', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(35, 'Piepi', 1, 0, 0, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(36, 'Pixi', 1, 0, 1, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(37, 'Vulpix', 1, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(38, 'Vulnona', 1, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(39, 'Pummeluff', 1, 0, 0, 'normal', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(40, 'Fluffeluff', 1, 0, 1, 'normal', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(41, 'Zubat', 1, 0, 0, 'gift', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(42, 'Golbat', 1, 0, 1, 'gift', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(43, 'Myrapla', 1, 0, 0, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(44, 'Duflor', 1, 0, 1, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(45, 'Giflor', 1, 0, 2, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(46, 'Paras', 1, 0, 0, 'käfer', 'pflanze')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(47, 'Parasek', 1, 0, 1, 'käfer', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(48, 'Bluzuk', 1, 0, 0, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(49, 'Omot', 1, 0, 1, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(50, 'Digda', 1, 0, 0, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(51, 'Digdri', 1, 0, 1, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(52, 'Mauzi', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(53, 'Snobilikat', 1, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(54, 'Enton', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(55, 'Entoron', 1, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(56, 'Menki', 1, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(57, 'Rasaff', 1, 0, 1, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(58, 'Fukano', 1, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(59, 'Arkani', 1, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(60, 'Quapsel', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(61, 'Quaputzi', 1, 0, 1, 'wasser')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(62, 'Quappo', 1, 0, 2, 'wasser', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(63, 'Abra', 1, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(64, 'Kadabra', 1, 0, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(65, 'Simsala', 1, 0, 2, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(66, 'Machollo', 1, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(67, 'Maschok', 1, 0, 1, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(68, 'Machomei', 1, 0, 2, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(69, 'Knofensa', 1, 0, 0, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(70, 'Ultrigaria', 1, 0, 1, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(71, 'Sarzenia', 1, 0, 2, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(72, 'Tentacha', 1, 0, 0, 'wasser', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(73, 'Tentoxa', 1, 0, 1, 'wasser', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(74, 'Kleinstein', 1, 0, 1, 'gestein', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(75, 'Georok', 1, 0, 1, 'gestein', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(76, 'Geowaz', 1, 0, 2, 'gestein', 'boden')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(77, 'Ponita', 1, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(78, 'Gallopa', 1, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(79, 'Flegmon', 1, 0, 0, 'wasser', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(80, 'Lahmus', 1, 0, 1, 'wasser', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(81, 'Magnetilo', 1, 0, 0, 'elektro', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(82, 'Magneton', 1, 0, 1, 'elektro', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(83, 'Porenta', 1, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(84, 'Dodu', 1, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(85, 'Dodri', 1, 0, 1, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(86, 'Jurob', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(87, 'Jugong', 1, 0, 1, 'wasser', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(88, 'Sleima', 1, 0, 0, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(89, 'Sleimok', 1, 0, 1, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(90, 'Muschas', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(91, 'Austos', 1, 0, 1, 'wasser', 'eis')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(92, 'Nebulak', 1, 0, 0, 'geist', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(93, 'Alpollo', 1, 0, 1, 'geist', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(94, 'Gengar', 1, 0, 2, 'geist', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(95, 'Onix', 1, 0, 0, 'gestein', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(96, 'Traumato', 1, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(97, 'Hypno', 1, 0, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(98, 'Krabby', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(99, 'Kingler', 1, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(100, 'Voltobal', 1, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(101, 'Lektrobal', 1, 0, 1, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(102, 'Owei', 1, 0, 0, 'pflanze', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(103, 'Kokowei', 1, 0, 1, 'pflanze', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(104, 'Tragosso', 1, 0, 0, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(105, 'Knogga', 1, 0, 1, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(106, 'Kicklee', 1, 0, 0, 'kampf')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(107, 'Nockchan', 1, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(108, 'Schlurp', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(109, 'Smogon', 1, 0, 0, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(110, 'Smogmog', 1, 0, 1, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(111, 'Rihorn', 1, 0, 0, 'boden', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(112, 'Rizeros', 1, 0, 1, 'boden', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(113, 'Chaneira', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(114, 'Tangela', 1, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(115, 'Kangama', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(116, 'Seeper', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(117, 'Seemon', 1, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(118, 'Goldini', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(119, 'Golking', 1, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(120, 'Sterndu', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(121, 'Starmie', 1, 0, 1, 'wasser', 'psycho')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(122, 'Pantimos', 1, 0, 0, 'psycho', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(123, 'Sichlor', 1, 0, 0, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(124, 'Rossana', 1, 0, 0, 'eis', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(125, 'Elektek', 1, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(126, 'Magmar', 1, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(127, 'Pinsir', 1, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(128, 'Tauros', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(129, 'Karpador', 1, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(130, 'Garados', 1, 0, 1, 'wasser', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(131, 'Lapras', 1, 0, 0, 'wasser', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(132, 'Ditto', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(133, 'Evoli', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(134, 'Aquana', 1, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(135, 'Blitza', 1, 0, 1, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(136, 'Flamara', 1, 0, 1, 'feuer')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(137, 'Porygon', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(138, 'Amonitas', 1, 0, 0, 'gestein', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(139, 'Amoroso', 1, 0, 1, 'gestein', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(140, 'Kabuto', 1, 0, 0, 'gestein', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(141, 'Kabutops', 1, 0, 1, 'gestein', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(142, 'Aerodactyl', 1, 0, 0, 'gestein', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(143, 'Relaxo', 1, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(144, 'Arktos', 1, 1, 0, 'eis', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(145, 'Zapdos', 1, 1, 0, 'elektro', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(146, 'Lavados', 1, 1, 0, 'feuer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(147, 'Dratini', 1, 0, 0, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(148, 'Dragonir', 1, 0, 1, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(149, 'Dragoran', 1, 0, 2, 'drache', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(150, 'Mewtu', 1, 1, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(151, 'Mew', 1, 2, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(152, 'Endivie', 2, 0, 0, 'pflanze')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(153, 'Lorblatt', 2, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(154, 'Meganie', 2, 0, 2, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(155, 'Feurigel', 2, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(156, 'Igelavar', 2, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(157, 'Tornupto', 2, 0, 2, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(158, 'Karnimani', 2, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(159, 'Tyracroc', 2, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(160, 'Impergator', 2, 0, 2, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(161, 'Wiesor', 2, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(162, 'Wiesenior', 2, 0, 1, 'normal')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(163, 'Hoothoot', 2, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(164, 'Noctuh', 2, 0, 1, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(165, 'Ledyba', 2, 0, 0, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(166, 'Ledian', 2, 0, 1, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(167, 'Webarak', 2, 0, 0, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(168, 'Ariados', 2, 0, 1, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(169, 'Iksbat', 2, 0, 2, 'gift', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(170, 'Lampi', 2, 0, 0, 'wasser', 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(171, 'Lanturn', 2, 0, 1, 'wasser', 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(172, 'Pichu', 2, 0, 3, 'elektro')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(173, 'Pii', 2, 0, 3, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(174, 'Fluffeluff', 2, 0, 3, 'normal', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(175, 'Togepi', 2, 0, 0, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(176, 'Togetic', 2, 0, 1, 'fee', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(177, 'Natu', 2, 0, 0, 'psycho', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(178, 'Xatu', 2, 0, 1, 'psycho', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(179, 'Voltilamm', 2, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(180, 'Waaty', 2, 0, 1, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(181, 'Ampharos', 2, 0, 2, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(182, 'Blubella', 2, 0, 2, 'pflanze')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(183, 'Marill', 2, 0, 0, 'wasser', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(184, 'Azumarill', 2, 0, 1, 'wasser', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(185, 'Mogelbaum', 2, 0, 0, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(186, 'Quaxo', 2, 0, 2, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(187, 'Hoppspross', 2, 0, 0, 'pflanze', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(188, 'Hubelupf', 2, 0, 1, 'pflanze', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(189, 'Papungha', 2, 0, 2, 'pflanze', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(190, 'Griffel', 2, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(191, 'Sonnkern', 2, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(192, 'Sonnflora', 2, 0, 1, 'pflanze')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(193, 'Yanma', 2, 0, 0, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(194, 'Felino', 2, 0, 0, 'wasser', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(195, 'Morlord', 2, 0, 1, 'wasser', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(196, 'Psiana', 2, 0, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(197, 'Nachtara', 2, 0, 1, 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(198, 'Kramurx', 2, 0, 0, 'unlicht', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(199, 'Laschoking', 2, 0, 1, 'wasser', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(200, 'Traunfugil', 2, 0, 0, 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(201, 'Icognito', 2, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(202, 'Woingenau', 2, 0, 0, 'psycho')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(203, 'Girafarig', 2, 0, 0, 'normal', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(204, 'Tanza', 2, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(205, 'Forstellka', 2, 0, 1, 'käfer', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(206, 'Dummisel', 2, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(207, 'Skorgla', 2, 0, 0, 'boden', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(208, 'Stahlos', 2, 0, 1, 'stahl', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(209, 'Snubbull', 2, 0, 0, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(210, 'Granbull', 2, 0, 1, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(211, 'Baldorfish', 2, 0, 0, 'wasser', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(212, 'Scherox', 2, 0, 1, 'käfer', 'stahl')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(213, 'Pottrott', 2, 0, 0, 'käfer', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(214, 'Skaraborn', 2, 0, 0, 'käfer', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(215, 'Sniebel', 2, 0, 0, 'unlicht', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(216, 'Teddiursa', 2, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(217, 'Ursaring', 2, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(218, 'Schneckmag', 2, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(219, 'Magcargo', 2, 0, 1, 'feuer', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(220, 'Quiekel', 2, 0, 0, 'eis', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(221, 'Keifel', 2, 0, 1, 'eis', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(222, 'Corasonn', 2, 0, 0, 'wasser', 'gestein')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(223, 'Remoraid', 2, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(224, 'Octillery', 2, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(225, 'Botogel', 2, 0, 0, 'eis', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(226, 'Mantax', 2, 0, 0, 'wasser', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(227, 'Panzaeron', 2, 0, 0, 'stahl', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(228, 'Hunduster', 2, 0, 0, 'unlicht', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(229, 'Hundemon', 2, 0, 1, 'unlicht', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(230, 'Seedraking', 2, 0, 2, 'wasser', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(231, 'Phanpy', 2, 0, 0, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(232, 'Donphan', 2, 0, 1, 'boden')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(233, 'Porygon2', 2, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(234, 'Damhirplex', 2, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(235, 'Farbeagle', 2, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(236, 'Rabauz', 2, 0, 3, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(237, 'Kapoera', 2, 0, 1, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(238, 'Kussilla', 2, 0, 3, 'eis', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(239, 'Elekid', 2, 0, 3, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(240, 'Magby', 2, 0, 3, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(241, 'Miltank', 2, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(242, 'Heiteira', 2, 0, 1, 'normal')"))
       qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(243, 'Raikou', 2, 1, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(244, 'Entei', 2, 1, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(245, 'Suicune', 2, 1, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(246, 'Larvitar', 2, 0, 0, 'gestein', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(247, 'Pupitar', 2, 0, 1, 'gestein', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(248, 'Despotar', 2, 0, 2, 'gestein', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(249, 'Lugia', 2, 1, 0, 'psycho', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(250, 'Ho-Oh', 2, 1, 0, 'feuer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(251, 'Celebi', 2, 2, 0, 'psycho', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(252, 'Geckarbor', 3, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(253, 'Reptain', 3, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(254, 'Gewaldro', 3, 0, 2, 'pflanze', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(255, 'Flemmli', 3, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(256, 'Jungglut', 3, 0, 1, 'feuer', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(257, 'Lohgock', 3, 0, 2, 'feuer', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(258, 'Hydropi', 3, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(259, 'Moorabbel', 3, 0, 1, 'wasser', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(260, 'Sumpex', 3, 0, 2, 'wasser', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(261, 'Fiffyen', 3, 0, 0, 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(262, 'Magnayen', 3, 0, 1, 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(263, 'Zigzachs', 3, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(264, 'Geradaks', 3, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(265, 'Waumpel', 3, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(266, 'Schaloko', 3, 0, 1, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(267, 'Papinella', 3, 0, 2, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(268, 'Panekon', 3, 0, 1, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(269, 'Pudox', 3, 0, 2, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(270, 'Loturzel', 3, 0, 0, 'wasser', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(271, 'Lombrero', 3, 0, 1, 'wasser', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(272, 'Kappalores', 3, 0, 2, 'wasser', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(273, 'Samurzel', 3, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(274, 'Blanas', 3, 0, 1, 'pflanze', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(275, 'Tengulist', 3, 0, 2, 'pflanze', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(276, 'Schwalbini', 3, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(277, 'Schwalboss', 3, 0, 1, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(278, 'Wingull', 3, 0, 0, 'wasser', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(279, 'Pelipper', 3, 0, 1, 'wasser', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(280, 'Trasla', 3, 0, 0, 'psycho', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(281, 'Kirlia', 3, 0, 1, 'psycho', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(282, 'Guardevoir', 3, 0, 2, 'psycho', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(283, 'Gehweiher', 3, 0, 0, 'käfer', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(284, 'Maskeregen', 3, 0, 1, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(285, 'Knilz', 3, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(286, 'Kapilz', 3, 0, 1, 'pflanze', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(287, 'Bummelz', 3, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(288, 'Muntier', 3, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(289, 'Letarking', 3, 0, 2, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(290, 'Nincada', 3, 0, 0, 'käfer', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(291, 'Ninjask', 3, 0, 1, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(292, 'Ninjatom', 3, 0, 1, 'käfer', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(293, 'Flurmel', 3, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(294, 'Krakeelo', 3, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(295, 'Krawumms', 3, 0, 2, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(296, 'Makuhita', 3, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(297, 'Hariyama', 3, 0, 1, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(298, 'Azurill', 3, 0, 3, 'normal', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(299, 'Nasgnet', 3, 0, 0, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(300, 'Eneco', 3, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(301, 'Enekoro', 3, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(302, 'Zobiris', 3, 0, 0, 'unlicht', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(303, 'Flunkifer', 3, 0, 0, 'stahl', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(304, 'Stollunior', 3, 0, 0, 'stahl', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(305, 'Stollrak', 3, 0, 1, 'stahl', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(306, 'Stolloss', 3, 0, 2, 'stahl', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(307, 'Meditie', 3, 0, 0, 'normal', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(308, 'Meditalis', 3, 0, 1, 'normal', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(309, 'Frizelbliz', 3, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(310, 'Voltenso', 3, 0, 1, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(311, 'Plusle', 3, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(312, 'Minun', 3, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(313, 'Volbeat', 3, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(314, 'Illumise', 3, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(315, 'Roselia', 3, 0, 0, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(316, 'Schluppuck', 3, 0, 0, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(317, 'Schluckwech', 3, 0, 1, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(318, 'Kanivanha', 3, 0, 0, 'wasser', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(319, 'Tohaido', 3, 0, 1, 'wasser', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(320, 'Wailmer', 3, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(321, 'Wailord', 3, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(322, 'Camaub', 3, 0, 0, 'feuer', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(323, 'Camerupt', 3, 0, 1, 'feuer', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(324, 'Qurtel', 3, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(325, 'Spoink', 3, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(326, 'Groink', 3, 0, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(327, 'Pandir', 3, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(328, 'Knacklion', 3, 0, 0, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(329, 'Vibrava', 3, 0, 1, 'boden', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(330, 'Libelldra', 3, 0, 2, 'boden', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(331, 'Tuska', 3, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(332, 'Noktuska', 3, 0, 1, 'pflanze', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(333, 'Wablu', 3, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(334, 'Altaria', 3, 0, 1, 'drache', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(335, 'Sengo', 3, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(336, 'Vipitis', 3, 0, 0, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(337, 'Lunastein', 3, 0, 0, 'gestein', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(338, 'Sonnfelr', 3, 0, 0, 'gestein', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(339, 'Schmerbe', 3, 0, 0, 'wasser', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(340, 'Welsar', 3, 0, 1, 'wasser', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(341, 'Krebscorps', 3, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(342, 'Krebutack', 3, 0, 1, 'wasser', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(343, 'Puppance', 3, 0, 0, 'boden', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(344, 'Lepumentas', 3, 0, 1, 'boden', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(345, 'Liliep', 3, 0, 0, 'gestein', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();


    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(346, 'Wielie', 3, 0, 1, 'gestein', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(347, 'Anorith', 3, 0, 0, 'gestein', 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(348, 'Armaldo', 3, 0, 1, 'normal', 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(349, 'Barschwa', 3, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(350, 'Milotic', 3, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(351, 'Formeo', 3, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(352, 'Kecleon', 3, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(353, 'Shuppet', 3, 0, 0, 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(354, 'Banette', 3, 0, 1, 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(355, 'Zwirrlicht', 3, 0, 0, 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(356, 'Zwirrklop', 3, 0, 1, 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(357, 'Tropius', 3, 0, 0, 'pflanze', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(358, 'Palimpalim', 3, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(359, 'Absol', 3, 0, 0, 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(360, 'Isso', 3, 0, 3, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(361, 'Schneppke', 3, 0, 0, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(362, 'Firnontor', 3, 0, 1, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(363, 'Seemops', 3, 0, 0, 'eis', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(364, 'Seejong', 3, 0, 1, 'eis', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(365, 'Walraisa', 3, 0, 2, 'eis', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(366, 'Perlu', 3, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(367, 'Aalabyss', 3, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(368, 'Saganabyss', 3, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(369, 'Relicanth', 3, 0, 0, 'wasser', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(370, 'Liebiskus', 3, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(371, 'Kindwurm', 3, 0, 0, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(372, 'Draschel', 3, 0, 1, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(373, 'Brutalanda', 3, 0, 2, 'drache', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(374, 'Tanhel', 3, 0, 0, 'stahl', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(375, 'Metang', 3, 0, 1, 'stahl', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(376, 'Metagross', 3, 0, 2, 'stahl', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(377, 'Regirock', 3, 1, 0, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(378, 'Regice', 3, 1, 0, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(379, 'Registeel', 3, 1, 0, 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(380, 'Latias', 3, 1, 0, 'drache', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(381, 'Latios', 3, 1, 0, 'drache', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(382, 'Kyogre', 3, 1, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(383, 'Groudon', 3, 1, 0, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(384, 'Rayquaza', 3, 1, 0, 'drache', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(385, 'Jirachi', 3, 2, 0, 'stahl', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(386, 'Deoxys', 3, 2, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();



    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(387, 'Chelast', 4, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(388, 'Chelcarain', 4, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(389, 'Chelterrar', 4, 0, 2, 'pflanze', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(390, 'Panflam', 4, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(391, 'Panpyro', 4, 0, 1, 'feuer', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(392, 'Panferno', 4, 0, 2, 'feuer', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(393, 'Plinfa', 4, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(394, 'Pilprin', 4, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(395, 'Impoleon', 4, 0, 2, 'wasser', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(396, 'Staralili', 4, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(397, 'Staravia', 4, 0, 1, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(398, 'Staraptor', 4, 0, 2, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(399, 'Bidiza', 4, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(400, 'Bidifas', 4, 0, 1, 'normal', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(401, 'Zirpurze', 4, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(402, 'Zirpeise', 4, 0, 1, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(403, 'Sheinux', 4, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(404, 'Luxio', 4, 0, 1, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(405, 'Luxtra', 4, 0, 2, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(406, 'Knospi', 4, 0, 3, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(407, 'Roserade', 4, 0, 1, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(408, 'Koknodon', 4, 0, 0, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(409, 'Rameidon', 4, 0, 1, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(410, 'Schilterus', 4, 0, 0, 'gestein', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(411, 'Bollterus', 4, 0, 1, 'gestein', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(412, 'Burmy', 4, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(413, 'Burmadame', 4, 0, 1, 'käfer', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(414, 'Moterpel', 4, 0, 1, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(415, 'Wadribie', 4, 0, 0, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(416, 'Honweisel', 4, 0, 1, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(417, 'Pachirisu', 4, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(418, 'Bamelin', 4, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(419, 'Bojelin', 4, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(420, 'Kikugi', 4, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(421, 'Kinoso', 4, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(422, 'Schalellos', 4, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(423, 'Gastrodon', 4, 0, 1, 'wasser', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(424, 'Ambidiffel', 4, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(425, 'Driftlon', 4, 0, 0, 'geist', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(426, 'Drifzepeli', 4, 0, 1, 'geist', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(427, 'Haspiror', 4, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(428, 'Schlapor', 4, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(429, 'Traunmagil', 4, 0, 1, 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(430, 'Kramshef', 4, 0, 1, 'unlicht', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(431, 'Charmian', 4, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(432, 'Shnurgarst', 4, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(433, 'Klingplim', 4, 0, 3, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(434, 'Skunkapuh', 4, 0, 0, 'gift', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(435, 'Skuntank', 4, 0, 1, 'gift', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(436, 'Bronzel', 4, 0, 0, 'stahl', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(437, 'Bronzong', 4, 0, 1, 'stahl', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(438, 'Mobai', 4, 0, 3, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(439, 'Pantimimi', 4, 0, 3, 'psycho', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(440, 'Wonneira', 4, 0, 3, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(441, 'Plaudagei', 4, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(442, 'Kryppuk', 4, 0, 0, 'geist', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(443, 'Kaumalat', 4, 0, 0, 'drache', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(444, 'Knarksel', 4, 0, 1, 'drache', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(445, 'Knakrack', 4, 0, 2, 'drache', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(446, 'Mampfaxo', 4, 0, 3, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(447, 'Riolu', 4, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(448, 'Lucario', 4, 0, 1, 'kampf', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(449, 'Hippopotas', 4, 0, 0, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(450, 'Hippoterus', 4, 0, 1, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(451, 'Pionskora', 4, 0, 0, 'gift', 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(452, 'Piondragi', 4, 0, 1, 'gift', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(453, 'Glibunkel', 4, 0, 0, 'gift', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(454, 'Toxiquak', 4, 0, 1, 'gift', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(455, 'Venuflibis', 4, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(456, 'Finneon', 4, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(457, 'Lumineon', 4, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(458, 'Mantirps', 4, 0, 3, 'wasser', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(459, 'Shnebedeck', 4, 0, 0, 'pflanze', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(460, 'Rexblisar', 4, 0, 1, 'pflanze', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(461, 'Snibunna', 4, 0, 1, 'unlicht', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(462, 'Magnezone', 4, 0, 1, 'elektro', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(463, 'Schlurplek', 4, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(464, 'Rihornior', 4, 0, 1, 'boden', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();



    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(465, 'Tangoloss', 4, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(466, 'Elevoltek', 4, 0, 1, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(467, 'Magbrant', 4, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(468, 'Togekiss', 4, 0, 1, 'fee', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(469, 'Yanmega', 4, 0, 1, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(470, 'Foliburba', 4, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(471, 'Glaziola', 4, 0, 1, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(472, 'Skorgro', 4, 0, 1, 'boden', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(473, 'Mamutel', 4, 0, 2, 'eis', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(474, 'Proygon-Z', 4, 0, 2, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(475, 'Galagladi', 4, 0, 2, 'psycho', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(476, 'Voluminas', 4, 0, 1, 'gestein', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(477, 'Zwirrfinst', 4, 0, 2, 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(478, 'Frosdedje', 4, 0, 1, 'eis', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(479, 'Rotom', 4, 0, 0, 'elektro', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();


    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(480, 'Selfe', 4, 1, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(481, 'Vesprit', 4, 1, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(482, 'Tobutz', 4, 1, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(483, 'Dialga', 4, 1, 0, 'stahl', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(484, 'Palkia', 4, 1, 0, 'wasser', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(485, 'Heatran', 4, 1, 0, 'feuer', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(486, 'Regigigas', 4, 1, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(487, 'Giratina', 4, 1, 0, 'geist', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(488, 'Cresselia', 4, 1, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(489, 'Phione', 4, 2, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(490, 'Manaphy', 4, 2, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(491, 'Darkrai', 4, 2, 0, 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(492, 'Shaymin', 4, 2, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(493, 'Arceus', 4, 2, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();




    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(494, 'Victini', 5, 2, 0, 'psycho', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(495, 'Serpifeu', 5, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(496, 'Efoserp', 5, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(497, 'Serpiroyal', 5, 0, 2, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(498, 'Floink', 5, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(499, 'Ferkokel', 5, 0, 1, 'feuer', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(500, 'Flambirex', 5, 0, 2, 'feuer', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(501, 'Ottaro', 5, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(502, 'Zwottronin', 5, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(503, 'Admural', 5, 0, 2, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(504, 'Nagelotz', 5, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(505, 'Kukmarda', 5, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(506, 'Yorkleff', 5, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(507, 'Terribark', 5, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(508, 'Bissbark', 5, 0, 2, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(509, 'Felilou', 5, 0, 0, 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(510, 'Kleoparda', 5, 0, 1, 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(511, 'Vegimak', 5, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(512, 'Vegichita', 5, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(513, 'Grillmak', 5, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(514, 'Grillchita', 5, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(515, 'Sodamak', 5, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(516, 'Sodachita', 5, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(517, 'Somniam', 5, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(518, 'Somnivora', 5, 0, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(519, 'Dusselgurr', 5, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(520, 'Navitaub', 5, 0, 1, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(521, 'Fasanob', 5, 0, 2, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(522, 'Elezeba', 5, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(523, 'Zebritz', 5, 0, 1, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(524, 'Kiesling', 5, 0, 0, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(525, 'Sedimantur', 5, 0, 1, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(526, 'Brockoloss', 5, 0, 2, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(527, 'Fleknoil', 5, 0, 0, 'psycho', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(528, 'Fletiamo', 5, 0, 1, 'psycho', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(529, 'Rotomurf', 5, 0, 0, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(530, 'Stalobor', 5, 0, 1, 'boden', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(531, 'Ohrdoch', 5, 0, 0, 'normal', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(532, 'Praktibalk', 5, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(533, 'Strepoli', 5, 0, 1, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(534, 'Meistagrif', 5, 0, 2, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(535, 'Schallquap', 5, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(536, 'Mebrana', 5, 0, 1, 'wasser', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(537, 'Branawarz', 5, 0, 2, 'wasser', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(538, 'Jiutetso', 5, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(539, 'Karadonis', 5, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(540, 'Strawickl', 5, 0, 0, 'käfer', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(541, 'Folikon', 5, 0, 1, 'käfer', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(542, 'Matrifol', 5, 0, 2, 'käfer', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(543, 'Toxiped', 5, 0, 0, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(544, 'Rollum', 5, 0, 1, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(545, 'Cerapendra', 5, 0, 2, 'käfer', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(546, 'Waumboll', 5, 0, 0, 'pflanze', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(547, 'Elfun', 5, 0, 1, 'pflanze', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(548, 'Lilminip', 5, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(549, 'Dressella', 5, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(550, 'Barschuft', 5, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(551, 'Ganovil', 5, 0, 0, 'boden', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(552, 'Rokkaiman', 5, 0, 1, 'boden', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(553, 'Rabigator', 5, 0, 2, 'boden', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(554, 'Flampion', 5, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(555, 'Flampivian', 5, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(556, 'Maracamba', 5, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(557, 'Lithomith', 5, 0, 0, 'käfer', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(558, 'Castellith', 5, 0, 1, 'käfer', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(559, 'Zurrokex', 5, 0, 0, 'unlicht', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(560, 'Irokex', 5, 0, 1, 'unlicht', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(561, 'Symvolara', 5, 0, 0, 'psycho', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(562, 'Makabaja', 5, 0, 0, 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(563, 'Echnatoll', 5, 0, 1, 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(564, 'Galapaflos', 5, 0, 0, 'wasser', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(565, 'Karippas', 5, 0, 1, 'wasser', 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(566, 'Flapteryx', 5, 0, 0, 'gestein', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(567, 'Aeropteryx', 5, 0, 1, 'gestein', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(568, 'Unratütox', 5, 0, 0, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(569, 'Deponitox', 5, 0, 1, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(570, 'Zorua', 5, 0, 0, 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(571, 'Zoroark', 5, 0, 1, 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(572, 'Picochilla', 5, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(573, 'Chillabell', 5, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(574, 'Mollimorba', 5, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(575, 'Hypnomorba', 5, 0, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(576, 'Morbitesse', 5, 0, 2, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(577, 'Monozyto', 5, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(578, 'Mitodos', 5, 0, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(579, 'Zytomega', 5, 0, 2, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(580, 'Piccolente', 5, 0, 0, 'wasser', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(581, 'Swaroness', 5, 0, 1, 'wasser', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(582, 'Gelatini', 5, 0, 0, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(583, 'Gelatroppo', 5, 0, 1, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(584, 'Gelatwino', 5, 0, 2, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(585, 'Sesokitz', 5, 0, 0, 'normal', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(586, 'Kronjuwild', 5, 0, 1, 'normal', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(587, 'Emolga', 5, 0, 0, 'elektro', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(588, 'Laukaps', 5, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(589, 'Cavalanzas', 5, 0, 1, 'käfer', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(590, 'Tarnpignon', 5, 0, 0, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(591, 'Hutsassa', 5, 0, 1, 'pflanze', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(592, 'Quabbel', 5, 0, 0, 'wasser', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(593, 'Aproquallyp', 5, 0, 1, 'wasser', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(594, 'Mamolida', 5, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(595, 'Wattzapf', 5, 0, 0, 'käfer', 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(596, 'Voltula', 5, 0, 1, 'käfer', 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(597, 'Kastadur', 5, 0, 0, 'pflanze', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(598, 'Tentantel', 5, 0, 1, 'pflanze', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(599, 'Klikk', 5, 0, 0, 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(600, 'Kliklak', 5, 0, 1, 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(601, 'Klikdiklak', 5, 0, 2, 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(602, 'Zapplardin', 5, 0, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(603, 'Zapplalek', 5, 0, 1, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(604, 'Zapplarang', 5, 0, 2, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(605, 'Pygraulon', 5, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(606, 'Megalon', 5, 0, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(607, 'Lichtel', 5, 0, 0, 'geist', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(608, 'Laternecto', 5, 0, 1, 'geist', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(609, 'Skelabra', 5, 0, 2, 'geist', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(610, 'Milza', 5, 0, 0, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(611, 'Sharfax', 5, 0, 1, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(612, 'Maxax', 5, 0, 2, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(613, 'Petznief', 5, 0, 0, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(614, 'Siberio', 5, 0, 1, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(615, 'Frigometri', 5, 0, 2, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(616, 'Schnuthelm', 5, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(617, 'Hydragil', 5, 0, 1, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(618, 'Flunschlik', 5, 0, 0, 'boden', 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(619, 'Lin-Fu', 5, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(620, 'Wie-Shu', 5, 0, 1, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(621, 'Shardrago', 5, 0, 0, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(622, 'Golbit', 5, 0, 0, 'boden', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(623, 'Golgantes', 5, 0, 1, 'boden', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(624, 'Gladiantri', 5, 0, 0, 'unlicht', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(625, 'Caesurio', 5, 0, 1, 'unlicht', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(626, 'Bisofank', 5, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(627, 'Geronimatz', 5, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(628, 'Washakwil', 5, 0, 1, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(629, 'Skallyk', 5, 0, 0, 'unlicht', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(630, 'Grypheldis', 5, 0, 1, 'unlicht', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(631, 'Furnifraß', 5, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(632, 'Fermicula', 5, 0, 0, 'käfer', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(633, 'Kapuno', 5, 0, 0, 'unlicht', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(634, 'Duodino', 5, 0, 1, 'unlicht', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(635, 'Trikephalo', 5, 0, 2, 'unlicht', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(636, 'Ignivor', 5, 0, 0, 'käfer', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(637, 'Ramoth', 5, 0, 1, 'käfer', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(638, 'Kobalium', 5, 1, 0, 'stahl', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(639, 'Terrakium', 5, 1, 0, 'gestein', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(640, 'Viridium', 5, 1, 0, 'pflanze', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(641, 'Boreos', 5, 1, 0, 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(642, 'Voltolos', 5, 1, 0, 'elektro', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(643, 'Reshiram', 5, 1, 0, 'drache', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(644, 'Zekrom', 5, 1, 0, 'drache', 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(645, 'Demeteros', 5, 1, 0, 'boden', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(646, 'Kyurem', 5, 1, 0, 'drache', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(647, 'Keldeo', 5, 2, 0, 'wasser', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(648, 'Meloetta', 5, 2, 0, 'normal', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(649, 'Genesect', 5, 2, 0, 'käfer', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();


    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(650, 'Igamaro', 6, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(651, 'Igastarnish', 6, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(652, 'Brigaron', 6, 0, 2, 'pflanze', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(653, 'Fynx', 6, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(654, 'Rutena', 6, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(655, 'Fennexis', 6, 0, 2, 'feuer', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(656, 'Froxy', 6, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(657, 'Amphizel', 6, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(658, 'Quajutsu', 6, 0, 2, 'wasser', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(659, 'Scoppel', 6, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(660, 'Grebbit', 6, 0, 1, 'normal', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(661, 'Dartiri', 6, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(662, 'Dartignis', 6, 0, 1, 'feuer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(663, 'Fiaro', 6, 0, 2, 'feuer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(664, 'Purmel', 6, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(665, 'Puponcho', 6, 0, 1, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(666, 'Vivillon', 6, 0, 2, 'käfer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(667, 'Leufeo', 6, 0, 0, 'feuer', 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(668, 'Pyroleo', 6, 0, 1, 'feuer', 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(669, 'Flabébé', 6, 0, 0, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(670, 'Floette', 6, 0, 1, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(671, 'Florges', 6, 0, 2, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(672, 'Mähikel', 6, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(673, 'Chevrumm', 6, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(674, 'Pam-Pam', 6, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(675, 'Pandrago', 6, 0, 1, 'kampf', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(676, 'Coifwaff', 6, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(677, 'Psiau', 6, 0, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(678, 'Psiaugon', 6, 0, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(679, 'Gramokles', 6, 0, 0, 'stahl', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(680, 'Duokles', 6, 0, 1, 'stahl', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(681, 'Durengard', 6, 0, 2, 'stahl', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(682, 'Parfi', 6, 0, 0, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(683, 'Parfinesse', 6, 0, 1, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(684, 'Flauschling', 6, 0, 0, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(685, 'Sabbaione', 6, 0, 1, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(686, 'Iscalar', 6, 0, 0, 'unlicht', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(687, 'Calamanero', 6, 0, 1, 'unlicht', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(688, 'Bithora', 6, 0, 0, 'gestein', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(689, 'Thanathora', 6, 0, 1, 'gestein', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(690, 'Algitt', 6, 0, 0, 'gift', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(691, 'Tandrak', 6, 0, 1, 'gift', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(692, 'Scampisto', 6, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(693, 'Wummer', 6, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(694, 'Eguana', 6, 0, 0, 'elektro', 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(695, 'Elezard', 6, 0, 1, 'elektro', 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(696, 'Balgoras', 6, 0, 0, 'gestein', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(697, 'Monagoras', 6, 0, 1, 'gestein', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(698, 'Amarino', 6, 0, 0, 'gestein', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(699, 'Amagarga', 6, 0, 1, 'gestein', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(700, 'feelinara', 6, 0, 1, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(701, 'Resladero', 6, 0, 0, 'kampf', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(702, 'Dedenne', 6, 0, 0, 'elektro', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(703, 'Rocara', 6, 0, 0, 'normal', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(704, 'Viscora', 6, 0, 0, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(705, 'Viscargot', 6, 0, 1, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(706, 'Viscogon', 6, 0, 2, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(707, 'Clavion', 6, 0, 0, 'stahl', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(708, 'Paragoni', 6, 0, 0, 'geist', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(709, 'Trombork', 6, 0, 0, 'geist', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(710, 'Irrbis', 6, 0, 0, 'geist', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(711, 'Pumpdjinn', 6, 0, 1, 'geist', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(712, 'Arktip', 6, 0, 0, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(713, 'Arktilas', 6, 0, 1, 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(714, 'eF-eM', 6, 0, 0, 'flug', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(715, 'UHaFnir', 6, 0, 1, 'flug', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(716, 'Xerneas', 6, 1, 0, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(717, 'Yveltal', 6, 1, 0, 'unlicht', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(718, 'Zygarde', 6, 1, 0, 'drache', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(719, 'Diancie', 6, 2, 0, 'gestein', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(720, 'Hoopa', 6, 2, 0, 'psycho', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(721, 'Volcanion', 6, 2, 0, 'feuer', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(722, 'Bauz', 7, 0, 0, 'pflanze', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(723, 'Arboretoss', 7, 0, 1, 'pflanze', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(724, 'Silvarro', 7, 0, 2, 'pflanze', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(725, 'Flamiau', 7, 0, 0, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(726, 'Miezunder', 7, 0, 1, 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(727, 'Fuegro', 7, 0, 2, 'feuer', 'unlicht')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(728, 'Robball', 7, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(729, 'Marikeck', 7, 0, 1, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(730, 'Primarene', 7, 0, 2, 'wasser', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(731, 'Peppeck', 7, 0, 0, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();


    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(732, 'Trompeck', 7, 0, 1, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(733, 'Tukanon', 7, 0, 2, 'normal', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(734, 'Mangunior', 7, 0, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(735, 'Manguspektor', 7, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(736, 'Mabula', 7, 0, 0, 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(737, 'Akkup', 7, 0, 1, 'käfer', 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(738, 'Donarion', 7, 0, 2, 'käfer', 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(739, 'Krabbox', 7, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(740, 'Krawell', 7, 0, 1, 'kampf', 'eis')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(741, 'Choreogel', 7, 0, 0, 'feuer', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(742, 'Wommel', 7, 0, 0, 'käfer', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(743, 'Bandelby', 7, 0, 1, 'käfer', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(744, 'Wuffels', 7, 0, 0, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(745, 'Wolwerock', 7, 0, 1, 'gestein')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(746, 'Lusardin', 7, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(747, 'Garstella', 7, 0, 1, 'gift', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(748, 'Aggrostella', 7, 0, 2, 'gift', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(749, 'Pampuli', 7, 0, 0, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(750, 'Pampross', 7, 0, 1, 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(751, 'Araqua', 7, 0, 0, 'wasser', 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(752, 'Aranestro', 7, 0, 1, 'wasser', 'käfer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(753, 'Imantis', 7, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(754, 'Mantidea', 7, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(755, 'Bubungus', 7, 0, 0, 'pflanze', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(756, 'Lamellux', 7, 0, 1, 'pflanze', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(757, 'Molunk', 7, 0, 0, 'gift', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(758, 'Amfira', 7, 0, 1, 'gift', 'feuer')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(759, 'Velursi', 7, 0, 0, 'normal', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(760, 'Kosturso', 7, 0, 1, 'normal', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(761, 'Frubberl', 7, 0, 0, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();


    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(762, 'Frubalia', 7, 0, 1, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(763, 'Fruyal', 7, 0, 2, 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(764, 'Curelei', 7, 0, 0, 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(765, 'Kommandutan', 7, 0, 0, 'normal', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(766, 'Quartermak', 7, 0, 0, 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(767, 'Reißlaus', 7, 0, 0, 'käfer', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(768, 'Tectass', 7, 0, 1, 'käfer', 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(769, 'Sankabuh', 7, 0, 0, 'geist', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(770, 'Colossand', 7, 0, 1, 'geist', 'boden')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(771, 'Gufa', 7, 0, 0, 'wasser')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();


    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(772, 'Typ:Null', 7, 1, 0, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(773, 'Amigento', 7, 1, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(774, 'Meteno', 7, 0, 0, 'gestein', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(775, 'Koalelu', 7, 0, 1, 'normal')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(776, 'Tortunator', 7, 0, 0, 'feuer', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(777, 'Togedemaru', 7, 0, 0, 'elektro', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(778, 'Mimigma', 7, 0, 0, 'geist', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(779, 'Knirfish', 7, 0, 0, 'wasser', 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(780, 'Sen-Long', 7, 0, 0, 'normal', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(781, 'Moruda', 7, 0, 0, 'geist', 'pflanze')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(782, 'Miniras', 7, 0, 0, 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(783, 'Mediras', 7, 0, 1, 'drache', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(784, 'Grandiras', 7, 0, 2, 'drache', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();

    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(785, 'Kapu-Riki', 7, 1, 0, 'elektro', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(786, 'Kapu-Fala', 7, 1, 0, 'psycho', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(787, 'Kapu-Toro', 7, 1, 0, 'pflanze', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(788, 'Kapu-Kime', 7, 1, 0, 'wasser', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(789, 'Cosmog', 7, 1, 0, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(790, 'Cosmovum', 7, 1, 1, 'psycho')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(791, 'Solgaleo', 7, 1, 0, 'psycho', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(792, 'Lunala', 7, 1, 0, 'psycho', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(793, 'Anego', 7, 3, 0, 'gestein', 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(794, 'Masskito', 7, 3, 0, 'käfer', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(795, 'Schabelle', 7, 3, 0, 'käfer', 'kampf')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(796, 'Voltriant', 7, 3, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(797, 'Kaguron', 7, 3, 0, 'stahl', 'flug')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(798, 'Katagami', 7, 3, 0, 'pflanze', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(799, 'Schlingking', 7, 3, 0, 'unlicht', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(800, 'Necrozma', 7, 3, 0, 'psycho', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(801, 'Magearna', 7, 2, 0, 'stahl', 'fee')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(802, 'Marshadow', 7, 2, 0, 'kampf', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(803, 'Venicro', 7, 3, 0, 'gift')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(804, 'Agoyon', 7, 3, 0, 'gift', 'drache')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(805, 'Muramura', 7, 3, 0, 'gestein', 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1, type2) VALUES(806, 'Kopplosio', 7, 3, 0, 'feuer', 'geist')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(807, 'Zeraora', 7, 2, 0, 'elektro')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(808, 'Meltan', 7, 2, 0, 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
    if(!query.exec("INSERT INTO pokedex(id, name, gen, legi, phase, type1) VALUES(809, 'Melmetal', 7, 2, 1, 'stahl')"))
        qWarning() << "MainWindow::DatabasePopulate - ERROR: " << query.lastError().text();
}

void Database::DatabasePopulate2()
{
//# 		Pokémon 	HP 	Attack 	Defense 	Sp. Attack 	Sp. Defense 	Speed 	Total 	Average
QSqlQuery queryy;

if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (1,'Bulbasaur','Grass','Poison',318,45,49,49,65,65,45,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (2,'Ivysaur','Grass','Poison',405,60,62,63,80,80,60,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (3,'Venusaur','Grass','Poison',525,80,82,83,100,100,80,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (4,'Charmander','Fire',NULL,309,39,52,43,60,50,65,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (5,'Charmeleon','Fire',NULL,405,58,64,58,80,65,80,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (6,'Charizard','Fire','Flying',534,78,84,78,109,85,100,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (7,'Squirtle','Water',NULL,314,44,48,65,50,64,43,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (8,'Wartortle','Water',NULL,405,59,63,80,65,80,58,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (9,'Blastoise','Water',NULL,530,79,83,100,85,105,78,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (10,'Caterpie','Bug',NULL,195,45,30,35,20,20,45,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (11,'Metapod','Bug',NULL,205,50,20,55,25,25,30,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (12,'Butterfree','Bug','Flying',395,60,45,50,90,80,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (13,'Weedle','Bug','Poison',195,40,35,30,20,20,50,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (14,'Kakuna','Bug','Poison',205,45,25,50,25,25,35,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (15,'Beedrill','Bug','Poison',395,65,90,40,45,80,75,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (16,'Pidgey','Normal','Flying',251,40,45,40,35,35,56,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (17,'Pidgeotto','Normal','Flying',349,63,60,55,50,50,71,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (18,'Pidgeot','Normal','Flying',479,83,80,75,70,70,101,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (19,'Rattata','Normal',NULL,253,30,56,35,25,35,72,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (20,'Raticate','Normal',NULL,413,55,81,60,50,70,97,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (21,'Spearow','Normal','Flying',262,40,60,30,31,31,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (22,'Fearow','Normal','Flying',442,65,90,65,61,61,100,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (23,'Ekans','Poison',NULL,288,35,60,44,40,54,55,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (24,'Arbok','Poison',NULL,438,60,85,69,65,79,80,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (25,'Pikachu','Electric',NULL,320,35,55,40,50,50,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (26,'Raichu','Electric',NULL,485,60,90,55,90,80,110,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (27,'Sandshrew','Ground',NULL,300,50,75,85,20,30,40,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (28,'Sandslash','Ground',NULL,450,75,100,110,45,55,65,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (29,'Nidoran♀','Poison',NULL,275,55,47,52,40,40,41,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (30,'Nidorina','Poison',NULL,365,70,62,67,55,55,56,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (31,'Nidoqueen','Poison','Ground',505,90,92,87,75,85,76,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (32,'Nidoran♂','Poison',NULL,273,46,57,40,40,40,50,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (33,'Nidorino','Poison',NULL,365,61,72,57,55,55,65,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (34,'Nidoking','Poison','Ground',505,81,102,77,85,75,85,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (35,'Clefairy','Fairy',NULL,323,70,45,48,60,65,35,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (36,'Clefable','Fairy',NULL,483,95,70,73,95,90,60,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (37,'Vulpix','Fire',NULL,299,38,41,40,50,65,65,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (38,'Ninetales','Fire',NULL,505,73,76,75,81,100,100,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (39,'Jigglypuff','Normal','Fairy',270,115,45,20,45,25,20,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (40,'Wigglytuff','Normal','Fairy',435,140,70,45,85,50,45,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (41,'Zubat','Poison','Flying',245,40,45,35,30,40,55,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (42,'Golbat','Poison','Flying',455,75,80,70,65,75,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (43,'Oddish','Grass','Poison',320,45,50,55,75,65,30,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (44,'Gloom','Grass','Poison',395,60,65,70,85,75,40,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (45,'Vileplume','Grass','Poison',490,75,80,85,110,90,50,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (46,'Paras','Bug','Grass',285,35,70,55,45,55,25,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (47,'Parasect','Bug','Grass',405,60,95,80,60,80,30,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (48,'Venonat','Bug','Poison',305,60,55,50,40,55,45,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (49,'Venomoth','Bug','Poison',450,70,65,60,90,75,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (50,'Diglett','Ground',NULL,265,10,55,25,35,45,95,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (51,'Dugtrio','Ground',NULL,405,35,80,50,50,70,120,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (52,'Meowth','Normal',NULL,290,40,45,35,40,40,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (53,'Persian','Normal',NULL,440,65,70,60,65,65,115,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (54,'Psyduck','Water',NULL,320,50,52,48,65,50,55,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (55,'Golduck','Water',NULL,500,80,82,78,95,80,85,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (56,'Mankey','Fighting',NULL,305,40,80,35,35,45,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (57,'Primeape','Fighting',NULL,455,65,105,60,60,70,95,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (58,'Growlithe','Fire',NULL,350,55,70,45,70,50,60,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (59,'Arcanine','Fire',NULL,555,90,110,80,100,80,95,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (60,'Poliwag','Water',NULL,300,40,50,40,40,40,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (61,'Poliwhirl','Water',NULL,385,65,65,65,50,50,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (62,'Poliwrath','Water','Fighting',510,90,95,95,70,90,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (63,'Abra','Psychic',NULL,310,25,20,15,105,55,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (64,'Kadabra','Psychic',NULL,400,40,35,30,120,70,105,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (65,'Alakazam','Psychic',NULL,500,55,50,45,135,95,120,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (66,'Machop','Fighting',NULL,305,70,80,50,35,35,35,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (67,'Machoke','Fighting',NULL,405,80,100,70,50,60,45,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (68,'Machamp','Fighting',NULL,505,90,130,80,65,85,55,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (69,'Bellsprout','Grass','Poison',300,50,75,35,70,30,40,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (70,'Weepinbell','Grass','Poison',390,65,90,50,85,45,55,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (71,'Victreebel','Grass','Poison',490,80,105,65,100,70,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (72,'Tentacool','Water','Poison',335,40,40,35,50,100,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (73,'Tentacruel','Water','Poison',515,80,70,65,80,120,100,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (74,'Geodude','Rock','Ground',300,40,80,100,30,30,20,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (75,'Graveler','Rock','Ground',390,55,95,115,45,45,35,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (76,'Golem','Rock','Ground',495,80,120,130,55,65,45,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (77,'Ponyta','Fire',NULL,410,50,85,55,65,65,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (78,'Rapidash','Fire',NULL,500,65,100,70,80,80,105,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (79,'Slowpoke','Water','Psychic',315,90,65,65,40,40,15,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (80,'Slowbro','Water','Psychic',490,95,75,110,100,80,30,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (81,'Magnemite','Electric','Steel',325,25,35,70,95,55,45,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (82,'Magneton','Electric','Steel',465,50,60,95,120,70,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (83,'Farfetch''d','Normal','Flying',352,52,65,55,58,62,60,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (84,'Doduo','Normal','Flying',310,35,85,45,35,35,75,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (85,'Dodrio','Normal','Flying',460,60,110,70,60,60,100,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (86,'Seel','Water',NULL,325,65,45,55,45,70,45,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (87,'Dewgong','Water','Ice',475,90,70,80,70,95,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (88,'Grimer','Poison',NULL,325,80,80,50,40,50,25,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (89,'Muk','Poison',NULL,500,105,105,75,65,100,50,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (90,'Shellder','Water',NULL,305,30,65,100,45,25,40,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (91,'Cloyster','Water','Ice',525,50,95,180,85,45,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (92,'Gastly','Ghost','Poison',310,30,35,30,100,35,80,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (93,'Haunter','Ghost','Poison',405,45,50,45,115,55,95,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (94,'Gengar','Ghost','Poison',500,60,65,60,130,75,110,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (95,'Onix','Rock','Ground',385,35,45,160,30,45,70,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (96,'Drowzee','Psychic',NULL,328,60,48,45,43,90,42,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (97,'Hypno','Psychic',NULL,483,85,73,70,73,115,67,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (98,'Krabby','Water',NULL,325,30,105,90,25,25,50,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (99,'Kingler','Water',NULL,475,55,130,115,50,50,75,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (100,'Voltorb','Electric',NULL,330,40,30,50,55,55,100,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (101,'Electrode','Electric',NULL,480,60,50,70,80,80,140,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (102,'Exeggcute','Grass','Psychic',325,60,40,80,60,45,40,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (103,'Exeggutor','Grass','Psychic',520,95,95,85,125,65,55,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (104,'Cubone','Ground',NULL,320,50,50,95,40,50,35,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (105,'Marowak','Ground',NULL,425,60,80,110,50,80,45,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (106,'Hitmonlee','Fighting',NULL,455,50,120,53,35,110,87,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (107,'Hitmonchan','Fighting',NULL,455,50,105,79,35,110,76,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (108,'Lickitung','Normal',NULL,385,90,55,75,60,75,30,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (109,'Koffing','Poison',NULL,340,40,65,95,60,45,35,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (110,'Weezing','Poison',NULL,490,65,90,120,85,70,60,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (111,'Rhyhorn','Ground','Rock',345,80,85,95,30,30,25,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (112,'Rhydon','Ground','Rock',485,105,130,120,45,45,40,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (113,'Chansey','Normal',NULL,450,250,5,5,35,105,50,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (114,'Tangela','Grass',NULL,435,65,55,115,100,40,60,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (115,'Kangaskhan','Normal',NULL,490,105,95,80,40,80,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (116,'Horsea','Water',NULL,295,30,40,70,70,25,60,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (117,'Seadra','Water',NULL,440,55,65,95,95,45,85,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (118,'Goldeen','Water',NULL,320,45,67,60,35,50,63,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (119,'Seaking','Water',NULL,450,80,92,65,65,80,68,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (120,'Staryu','Water',NULL,340,30,45,55,70,55,85,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (121,'Starmie','Water','Psychic',520,60,75,85,100,85,115,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (122,'Mr. Mime','Psychic','Fairy',460,40,45,65,100,120,90,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (123,'Scyther','Bug','Flying',500,70,110,80,55,80,105,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (124,'Jynx','Ice','Psychic',455,65,50,35,115,95,95,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (125,'Electabuzz','Electric',NULL,490,65,83,57,95,85,105,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (126,'Magmar','Fire',NULL,495,65,95,57,100,85,93,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (127,'Pinsir','Bug',NULL,500,65,125,100,55,70,85,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (128,'Tauros','Normal',NULL,490,75,100,95,40,70,110,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (129,'Magikarp','Water',NULL,200,20,10,55,15,20,80,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (130,'Gyarados','Water','Flying',540,95,125,79,60,100,81,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (131,'Lapras','Water','Ice',535,130,85,80,85,95,60,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (132,'Ditto','Normal',NULL,288,48,48,48,48,48,48,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (133,'Eevee','Normal',NULL,325,55,55,50,45,65,55,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (134,'Vaporeon','Water',NULL,525,130,65,60,110,95,65,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (135,'Jolteon','Electric',NULL,525,65,65,60,110,95,130,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (136,'Flareon','Fire',NULL,525,65,130,60,95,110,65,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (137,'Porygon','Normal',NULL,395,65,60,70,85,75,40,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (138,'Omanyte','Rock','Water',355,35,40,100,90,55,35,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (139,'Omastar','Rock','Water',495,70,60,125,115,70,55,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (140,'Kabuto','Rock','Water',355,30,80,90,55,45,55,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (141,'Kabutops','Rock','Water',495,60,115,105,65,70,80,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (142,'Aerodactyl','Rock','Flying',515,80,105,65,60,75,130,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (143,'Snorlax','Normal',NULL,540,160,110,65,65,110,30,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (144,'Articuno','Ice','Flying',580,90,85,100,95,125,85,1,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (145,'Zapdos','Electric','Flying',580,90,90,85,125,90,100,1,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (146,'Moltres','Fire','Flying',580,90,100,90,125,85,90,1,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (147,'Dratini','Dragon',NULL,300,41,64,45,50,50,50,1,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (148,'Dragonair','Dragon',NULL,420,61,84,65,70,70,70,1,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (149,'Dragonite','Dragon','Flying',600,91,134,95,100,100,80,1,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (150,'Mewtwo','Psychic',NULL,680,106,110,90,154,90,130,1,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (151,'Mew','Psychic',NULL,600,100,100,100,100,100,100,1,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (152,'Chikorita','Grass',NULL,318,45,49,65,49,65,45,2,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (153,'Bayleef','Grass',NULL,405,60,62,80,63,80,60,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (154,'Meganium','Grass',NULL,525,80,82,100,83,100,80,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (155,'Cyndaquil','Fire',NULL,309,39,52,43,60,50,65,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (156,'Quilava','Fire',NULL,405,58,64,58,80,65,80,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (157,'Typhlosion','Fire',NULL,534,78,84,78,109,85,100,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (158,'Totodile','Water',NULL,314,50,65,64,44,48,43,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (159,'Croconaw','Water',NULL,405,65,80,80,59,63,58,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (160,'Feraligatr','Water',NULL,530,85,105,100,79,83,78,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (161,'Sentret','Normal',NULL,215,35,46,34,35,45,20,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (162,'Furret','Normal',NULL,415,85,76,64,45,55,90,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (163,'Hoothoot','Normal','Flying',262,60,30,30,36,56,50,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (164,'Noctowl','Normal','Flying',442,100,50,50,76,96,70,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (165,'Ledyba','Bug','Flying',265,40,20,30,40,80,55,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (166,'Ledian','Bug','Flying',390,55,35,50,55,110,85,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (167,'Spinarak','Bug','Poison',250,40,60,40,40,40,30,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (168,'Ariados','Bug','Poison',390,70,90,70,60,60,40,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (169,'Crobat','Poison','Flying',535,85,90,80,70,80,130,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (170,'Chinchou','Water','Electric',330,75,38,38,56,56,67,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (171,'Lanturn','Water','Electric',460,125,58,58,76,76,67,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (172,'Pichu','Electric',NULL,205,20,40,15,35,35,60,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (173,'Cleffa','Fairy',NULL,218,50,25,28,45,55,15,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (174,'Igglybuff','Normal','Fairy',210,90,30,15,40,20,15,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (175,'Togepi','Fairy',NULL,245,35,20,65,40,65,20,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (176,'Togetic','Fairy','Flying',405,55,40,85,80,105,40,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (177,'Natu','Psychic','Flying',320,40,50,45,70,45,70,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (178,'Xatu','Psychic','Flying',470,65,75,70,95,70,95,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (179,'Mareep','Electric',NULL,280,55,40,40,65,45,35,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (180,'Flaaffy','Electric',NULL,365,70,55,55,80,60,45,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (181,'Ampharos','Electric',NULL,510,90,75,85,115,90,55,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (182,'Bellossom','Grass',NULL,490,75,80,95,90,100,50,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (183,'Marill','Water','Fairy',250,70,20,50,20,50,40,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (184,'Azumarill','Water','Fairy',420,100,50,80,60,80,50,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (185,'Sudowoodo','Rock',NULL,410,70,100,115,30,65,30,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (186,'Politoed','Water',NULL,500,90,75,75,90,100,70,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (187,'Hoppip','Grass','Flying',250,35,35,40,35,55,50,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (188,'Skiploom','Grass','Flying',340,55,45,50,45,65,80,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (189,'Jumpluff','Grass','Flying',460,75,55,70,55,95,110,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (190,'Aipom','Normal',NULL,360,55,70,55,40,55,85,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (191,'Sunkern','Grass',NULL,180,30,30,30,30,30,30,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (192,'Sunflora','Grass',NULL,425,75,75,55,105,85,30,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (193,'Yanma','Bug','Flying',390,65,65,45,75,45,95,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (194,'Wooper','Water','Ground',210,55,45,45,25,25,15,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (195,'Quagsire','Water','Ground',430,95,85,85,65,65,35,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (196,'Espeon','Psychic',NULL,525,65,65,60,130,95,110,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (197,'Umbreon','Dark',NULL,525,95,65,110,60,130,65,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (198,'Murkrow','Dark','Flying',405,60,85,42,85,42,91,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (199,'Slowking','Water','Psychic',490,95,75,80,100,110,30,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (200,'Misdreavus','Ghost',NULL,435,60,60,60,85,85,85,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (201,'Unown','Psychic',NULL,336,48,72,48,72,48,48,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (202,'Wobbuffet','Psychic',NULL,405,190,33,58,33,58,33,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (203,'Girafarig','Normal','Psychic',455,70,80,65,90,65,85,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (204,'Pineco','Bug',NULL,290,50,65,90,35,35,15,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (205,'Forretress','Bug','Steel',465,75,90,140,60,60,40,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (206,'Dunsparce','Normal',NULL,415,100,70,70,65,65,45,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (207,'Gligar','Ground','Flying',430,65,75,105,35,65,85,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (208,'Steelix','Steel','Ground',510,75,85,200,55,65,30,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (209,'Snubbull','Fairy',NULL,300,60,80,50,40,40,30,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (210,'Granbull','Fairy',NULL,450,90,120,75,60,60,45,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (211,'Qwilfish','Water','Poison',430,65,95,75,55,55,85,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (212,'Scizor','Bug','Steel',500,70,130,100,55,80,65,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (213,'Shuckle','Bug','Rock',505,20,10,230,10,230,5,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (214,'Heracross','Bug','Fighting',500,80,125,75,40,95,85,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (215,'Sneasel','Dark','Ice',430,55,95,55,35,75,115,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (216,'Teddiursa','Normal',NULL,330,60,80,50,50,50,40,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (217,'Ursaring','Normal',NULL,500,90,130,75,75,75,55,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (218,'Slugma','Fire',NULL,250,40,40,40,70,40,20,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (219,'Magcargo','Fire','Rock',410,50,50,120,80,80,30,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (220,'Swinub','Ice','Ground',250,50,50,40,30,30,50,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (221,'Piloswine','Ice','Ground',450,100,100,80,60,60,50,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (222,'Corsola','Water','Rock',380,55,55,85,65,85,35,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (223,'Remoraid','Water',NULL,300,35,65,35,65,35,65,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (224,'Octillery','Water',NULL,480,75,105,75,105,75,45,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (225,'Delibird','Ice','Flying',330,45,55,45,65,45,75,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (226,'Mantine','Water','Flying',465,65,40,70,80,140,70,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (227,'Skarmory','Steel','Flying',465,65,80,140,40,70,70,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (228,'Houndour','Dark','Fire',330,45,60,30,80,50,65,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (229,'Houndoom','Dark','Fire',500,75,90,50,110,80,95,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (230,'Kingdra','Water','Dragon',540,75,95,95,95,95,85,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (231,'Phanpy','Ground',NULL,330,90,60,60,40,40,40,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (232,'Donphan','Ground',NULL,500,90,120,120,60,60,50,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (233,'Porygon2','Normal',NULL,515,85,80,90,105,95,60,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (234,'Stantler','Normal',NULL,465,73,95,62,85,65,85,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (235,'Smeargle','Normal',NULL,250,55,20,35,20,45,75,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (236,'Tyrogue','Fighting',NULL,210,35,35,35,35,35,35,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (237,'Hitmontop','Fighting',NULL,455,50,95,95,35,110,70,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (238,'Smoochum','Ice','Psychic',305,45,30,15,85,65,65,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (239,'Elekid','Electric',NULL,360,45,63,37,65,55,95,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (240,'Magby','Fire',NULL,365,45,75,37,70,55,83,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (241,'Miltank','Normal',NULL,490,95,80,105,40,70,100,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (242,'Blissey','Normal',NULL,540,255,10,10,75,135,55,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (243,'Raikou','Electric',NULL,580,90,85,75,115,100,115,2,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (244,'Entei','Fire',NULL,580,115,115,85,90,75,100,2,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (245,'Suicune','Water',NULL,580,100,75,115,90,115,85,2,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (246,'Larvitar','Rock','Ground',300,50,64,50,45,50,41,2,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (247,'Pupitar','Rock','Ground',410,70,84,70,65,70,51,2,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (248,'Tyranitar','Rock','Dark',600,100,134,110,95,100,61,2,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (249,'Lugia','Psychic','Flying',680,106,90,130,90,154,110,2,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (250,'Ho-oh','Fire','Flying',680,106,130,90,110,154,90,2,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (251,'Celebi','Psychic','Grass',600,100,100,100,100,100,100,2,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (252,'Treecko','Grass',NULL,310,40,45,35,65,55,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (253,'Grovyle','Grass',NULL,405,50,65,45,85,65,95,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (254,'Sceptile','Grass',NULL,530,70,85,65,105,85,120,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (255,'Torchic','Fire',NULL,310,45,60,40,70,50,45,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (256,'Combusken','Fire','Fighting',405,60,85,60,85,60,55,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (257,'Blaziken','Fire','Fighting',530,80,120,70,110,70,80,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (258,'Mudkip','Water',NULL,310,50,70,50,50,50,40,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (259,'Marshtomp','Water','Ground',405,70,85,70,60,70,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (260,'Swampert','Water','Ground',535,100,110,90,85,90,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (261,'Poochyena','Dark',NULL,220,35,55,35,30,30,35,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (262,'Mightyena','Dark',NULL,420,70,90,70,60,60,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (263,'Zigzagoon','Normal',NULL,240,38,30,41,30,41,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (264,'Linoone','Normal',NULL,420,78,70,61,50,61,100,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (265,'Wurmple','Bug',NULL,195,45,45,35,20,30,20,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (266,'Silcoon','Bug',NULL,205,50,35,55,25,25,15,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (267,'Beautifly','Bug','Flying',395,60,70,50,100,50,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (268,'Cascoon','Bug',NULL,205,50,35,55,25,25,15,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (269,'Dustox','Bug','Poison',385,60,50,70,50,90,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (270,'Lotad','Water','Grass',220,40,30,30,40,50,30,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (271,'Lombre','Water','Grass',340,60,50,50,60,70,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (272,'Ludicolo','Water','Grass',480,80,70,70,90,100,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (273,'Seedot','Grass',NULL,220,40,40,50,30,30,30,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (274,'Nuzleaf','Grass','Dark',340,70,70,40,60,40,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (275,'Shiftry','Grass','Dark',480,90,100,60,90,60,80,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (276,'Taillow','Normal','Flying',270,40,55,30,30,30,85,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (277,'Swellow','Normal','Flying',430,60,85,60,50,50,125,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (278,'Wingull','Water','Flying',270,40,30,30,55,30,85,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (279,'Pelipper','Water','Flying',430,60,50,100,85,70,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (280,'Ralts','Psychic','Fairy',198,28,25,25,45,35,40,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (281,'Kirlia','Psychic','Fairy',278,38,35,35,65,55,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (282,'Gardevoir','Psychic','Fairy',518,68,65,65,125,115,80,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (283,'Surskit','Bug','Water',269,40,30,32,50,52,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (284,'Masquerain','Bug','Flying',414,70,60,62,80,82,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (285,'Shroomish','Grass',NULL,295,60,40,60,40,60,35,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (286,'Breloom','Grass','Fighting',460,60,130,80,60,60,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (287,'Slakoth','Normal',NULL,280,60,60,60,35,35,30,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (288,'Vigoroth','Normal',NULL,440,80,80,80,55,55,90,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (289,'Slaking','Normal',NULL,670,150,160,100,95,65,100,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (290,'Nincada','Bug','Ground',266,31,45,90,30,30,40,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (291,'Ninjask','Bug','Flying',456,61,90,45,50,50,160,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (292,'Shedinja','Bug','Ghost',236,1,90,45,30,30,40,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (293,'Whismur','Normal',NULL,240,64,51,23,51,23,28,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (294,'Loudred','Normal',NULL,360,84,71,43,71,43,48,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (295,'Exploud','Normal',NULL,490,104,91,63,91,73,68,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (296,'Makuhita','Fighting',NULL,237,72,60,30,20,30,25,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (297,'Hariyama','Fighting',NULL,474,144,120,60,40,60,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (298,'Azurill','Normal','Fairy',190,50,20,40,20,40,20,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (299,'Nosepass','Rock',NULL,375,30,45,135,45,90,30,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (300,'Skitty','Normal',NULL,260,50,45,45,35,35,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (301,'Delcatty','Normal',NULL,380,70,65,65,55,55,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (302,'Sableye','Dark','Ghost',380,50,75,75,65,65,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (303,'Mawile','Steel','Fairy',380,50,85,85,55,55,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (304,'Aron','Steel','Rock',330,50,70,100,40,40,30,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (305,'Lairon','Steel','Rock',430,60,90,140,50,50,40,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (306,'Aggron','Steel','Rock',530,70,110,180,60,60,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (307,'Meditite','Fighting','Psychic',280,30,40,55,40,55,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (308,'Medicham','Fighting','Psychic',410,60,60,75,60,75,80,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (309,'Electrike','Electric',NULL,295,40,45,40,65,40,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (310,'Manectric','Electric',NULL,475,70,75,60,105,60,105,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (311,'Plusle','Electric',NULL,405,60,50,40,85,75,95,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (312,'Minun','Electric',NULL,405,60,40,50,75,85,95,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (313,'Volbeat','Bug',NULL,400,65,73,55,47,75,85,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (314,'Illumise','Bug',NULL,400,65,47,55,73,75,85,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (315,'Roselia','Grass','Poison',400,50,60,45,100,80,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (316,'Gulpin','Poison',NULL,302,70,43,53,43,53,40,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (317,'Swalot','Poison',NULL,467,100,73,83,73,83,55,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (318,'Carvanha','Water','Dark',305,45,90,20,65,20,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (319,'Sharpedo','Water','Dark',460,70,120,40,95,40,95,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (320,'Wailmer','Water',NULL,400,130,70,35,70,35,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (321,'Wailord','Water',NULL,500,170,90,45,90,45,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (322,'Numel','Fire','Ground',305,60,60,40,65,45,35,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (323,'Camerupt','Fire','Ground',460,70,100,70,105,75,40,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (324,'Torkoal','Fire',NULL,470,70,85,140,85,70,20,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (325,'Spoink','Psychic',NULL,330,60,25,35,70,80,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (326,'Grumpig','Psychic',NULL,470,80,45,65,90,110,80,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (327,'Spinda','Normal',NULL,360,60,60,60,60,60,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (328,'Trapinch','Ground',NULL,290,45,100,45,45,45,10,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (329,'Vibrava','Ground','Dragon',340,50,70,50,50,50,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (330,'Flygon','Ground','Dragon',520,80,100,80,80,80,100,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (331,'Cacnea','Grass',NULL,335,50,85,40,85,40,35,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (332,'Cacturne','Grass','Dark',475,70,115,60,115,60,55,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (333,'Swablu','Normal','Flying',310,45,40,60,40,75,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (334,'Altaria','Dragon','Flying',490,75,70,90,70,105,80,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (335,'Zangoose','Normal',NULL,458,73,115,60,60,60,90,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (336,'Seviper','Poison',NULL,458,73,100,60,100,60,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (337,'Lunatone','Rock','Psychic',440,70,55,65,95,85,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (338,'Solrock','Rock','Psychic',440,70,95,85,55,65,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (339,'Barboach','Water','Ground',288,50,48,43,46,41,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (340,'Whiscash','Water','Ground',468,110,78,73,76,71,60,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (341,'Corphish','Water',NULL,308,43,80,65,50,35,35,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (342,'Crawdaunt','Water','Dark',468,63,120,85,90,55,55,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (343,'Baltoy','Ground','Psychic',300,40,40,55,40,70,55,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (344,'Claydol','Ground','Psychic',500,60,70,105,70,120,75,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (345,'Lileep','Rock','Grass',355,66,41,77,61,87,23,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (346,'Cradily','Rock','Grass',495,86,81,97,81,107,43,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (347,'Anorith','Rock','Bug',355,45,95,50,40,50,75,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (348,'Armaldo','Rock','Bug',495,75,125,100,70,80,45,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (349,'Feebas','Water',NULL,200,20,15,20,10,55,80,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (350,'Milotic','Water',NULL,540,95,60,79,100,125,81,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (351,'Castform','Normal',NULL,420,70,70,70,70,70,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (352,'Kecleon','Normal',NULL,440,60,90,70,60,120,40,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (353,'Shuppet','Ghost',NULL,295,44,75,35,63,33,45,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (354,'Banette','Ghost',NULL,455,64,115,65,83,63,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (355,'Duskull','Ghost',NULL,295,20,40,90,30,90,25,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (356,'Dusclops','Ghost',NULL,455,40,70,130,60,130,25,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (357,'Tropius','Grass','Flying',460,99,68,83,72,87,51,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (358,'Chimecho','Psychic',NULL,425,65,50,70,95,80,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (359,'Absol','Dark',NULL,465,65,130,60,75,60,75,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (360,'Wynaut','Psychic',NULL,260,95,23,48,23,48,23,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (361,'Snorunt','Ice',NULL,300,50,50,50,50,50,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (362,'Glalie','Ice',NULL,480,80,80,80,80,80,80,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (363,'Spheal','Ice','Water',290,70,40,50,55,50,25,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (364,'Sealeo','Ice','Water',410,90,60,70,75,70,45,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (365,'Walrein','Ice','Water',530,110,80,90,95,90,65,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (366,'Clamperl','Water',NULL,345,35,64,85,74,55,32,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (367,'Huntail','Water',NULL,485,55,104,105,94,75,52,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (368,'Gorebyss','Water',NULL,485,55,84,105,114,75,52,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (369,'Relicanth','Water','Rock',485,100,90,130,45,65,55,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (370,'Luvdisc','Water',NULL,330,43,30,55,40,65,97,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (371,'Bagon','Dragon',NULL,300,45,75,60,40,30,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (372,'Shelgon','Dragon',NULL,420,65,95,100,60,50,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (373,'Salamence','Dragon','Flying',600,95,135,80,110,80,100,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();

if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (374,'Beldum','Steel','Psychic',300,40,55,80,35,60,30,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (375,'Metang','Steel','Psychic',420,60,75,100,55,80,50,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (376,'Metagross','Steel','Psychic',600,80,135,130,95,90,70,3,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();

if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (377,'Regirock','Rock',NULL,580,80,100,200,50,100,50,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (378,'Regice','Ice',NULL,580,80,50,100,100,200,50,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (379,'Registeel','Steel',NULL,580,80,75,150,75,150,50,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (380,'Latias','Dragon','Psychic',600,80,80,90,110,130,110,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (381,'Latios','Dragon','Psychic',600,80,90,80,130,110,110,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (382,'Kyogre','Water',NULL,670,100,100,90,150,140,90,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (383,'Groudon','Ground',NULL,670,100,150,140,100,90,90,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (384,'Rayquaza','Dragon','Flying',680,105,150,90,150,90,95,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (385,'Jirachi','Steel','Psychic',600,100,100,100,100,100,100,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (386,'DeoxysNormal Forme','Psychic',NULL,600,50,150,50,150,50,150,3,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (387,'Turtwig','Grass',NULL,318,55,68,64,45,55,31,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (388,'Grotle','Grass',NULL,405,75,89,85,55,65,36,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (389,'Torterra','Grass','Ground',525,95,109,105,75,85,56,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (390,'Chimchar','Fire',NULL,309,44,58,44,58,44,61,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (391,'Monferno','Fire','Fighting',405,64,78,52,78,52,81,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (392,'Infernape','Fire','Fighting',534,76,104,71,104,71,108,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (393,'Piplup','Water',NULL,314,53,51,53,61,56,40,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (394,'Prinplup','Water',NULL,405,64,66,68,81,76,50,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (395,'Empoleon','Water','Steel',530,84,86,88,111,101,60,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (396,'Starly','Normal','Flying',245,40,55,30,30,30,60,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (397,'Staravia','Normal','Flying',340,55,75,50,40,40,80,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (398,'Staraptor','Normal','Flying',485,85,120,70,50,60,100,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (399,'Bidoof','Normal',NULL,250,59,45,40,35,40,31,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (400,'Bibarel','Normal','Water',410,79,85,60,55,60,71,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (401,'Kricketot','Bug',NULL,194,37,25,41,25,41,25,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (402,'Kricketune','Bug',NULL,384,77,85,51,55,51,65,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (403,'Shinx','Electric',NULL,263,45,65,34,40,34,45,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (404,'Luxio','Electric',NULL,363,60,85,49,60,49,60,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (405,'Luxray','Electric',NULL,523,80,120,79,95,79,70,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (406,'Budew','Grass','Poison',280,40,30,35,50,70,55,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (407,'Roserade','Grass','Poison',515,60,70,65,125,105,90,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (408,'Cranidos','Rock',NULL,350,67,125,40,30,30,58,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (409,'Rampardos','Rock',NULL,495,97,165,60,65,50,58,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (410,'Shieldon','Rock','Steel',350,30,42,118,42,88,30,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (411,'Bastiodon','Rock','Steel',495,60,52,168,47,138,30,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (412,'Burmy','Bug',NULL,224,40,29,45,29,45,36,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (413,'WormadamPlant Cloak','Bug','Grass',424,60,59,85,79,105,36,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (414,'Mothim','Bug','Flying',424,70,94,50,94,50,66,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (415,'Combee','Bug','Flying',244,30,30,42,30,42,70,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (416,'Vespiquen','Bug','Flying',474,70,80,102,80,102,40,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (417,'Pachirisu','Electric',NULL,405,60,45,70,45,90,95,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (418,'Buizel','Water',NULL,330,55,65,35,60,30,85,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (419,'Floatzel','Water',NULL,495,85,105,55,85,50,115,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (420,'Cherubi','Grass',NULL,275,45,35,45,62,53,35,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (421,'Cherrim','Grass',NULL,450,70,60,70,87,78,85,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (422,'Shellos','Water',NULL,325,76,48,48,57,62,34,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (423,'Gastrodon','Water','Ground',475,111,83,68,92,82,39,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (424,'Ambipom','Normal',NULL,482,75,100,66,60,66,115,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (425,'Drifloon','Ghost','Flying',348,90,50,34,60,44,70,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (426,'Drifblim','Ghost','Flying',498,150,80,44,90,54,80,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (427,'Buneary','Normal',NULL,350,55,66,44,44,56,85,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (428,'Lopunny','Normal',NULL,480,65,76,84,54,96,105,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();

if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (429,'Mismagius','Ghost',NULL,495,60,60,60,105,105,105,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (430,'Honchkrow','Dark','Flying',505,100,125,52,105,52,71,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (431,'Glameow','Normal',NULL,310,49,55,42,42,37,85,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (432,'Purugly','Normal',NULL,452,71,82,64,64,59,112,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (433,'Chingling','Psychic',NULL,285,45,30,50,65,50,45,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (434,'Stunky','Poison','Dark',329,63,63,47,41,41,74,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (435,'Skuntank','Poison','Dark',479,103,93,67,71,61,84,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (436,'Bronzor','Steel','Psychic',300,57,24,86,24,86,23,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (437,'Bronzong','Steel','Psychic',500,67,89,116,79,116,33,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (438,'Bonsly','Rock',NULL,290,50,80,95,10,45,10,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (439,'Mime Jr.','Psychic','Fairy',310,20,25,45,70,90,60,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (440,'Happiny','Normal',NULL,220,100,5,5,15,65,30,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (441,'Chatot','Normal','Flying',411,76,65,45,92,42,91,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (442,'Spiritomb','Ghost','Dark',485,50,92,108,92,108,35,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (443,'Gible','Dragon','Ground',300,58,70,45,40,45,42,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (444,'Gabite','Dragon','Ground',410,68,90,65,50,55,82,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (445,'Garchomp','Dragon','Ground',600,108,130,95,80,85,102,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (446,'Munchlax','Normal',NULL,390,135,85,40,40,85,5,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (447,'Riolu','Fighting',NULL,285,40,70,40,35,40,60,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (448,'Lucario','Fighting','Steel',525,70,110,70,115,70,90,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();

if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (449,'Hippopotas','Ground',NULL,330,68,72,78,38,42,32,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (450,'Hippowdon','Ground',NULL,525,108,112,118,68,72,47,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (451,'Skorupi','Poison','Bug',330,40,50,90,30,55,65,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (452,'Drapion','Poison','Dark',500,70,90,110,60,75,95,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (453,'Croagunk','Poison','Fighting',300,48,61,40,61,40,50,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (454,'Toxicroak','Poison','Fighting',490,83,106,65,86,65,85,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (455,'Carnivine','Grass',NULL,454,74,100,72,90,72,46,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (456,'Finneon','Water',NULL,330,49,49,56,49,61,66,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (457,'Lumineon','Water',NULL,460,69,69,76,69,86,91,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (458,'Mantyke','Water','Flying',345,45,20,50,60,120,50,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (459,'Snover','Grass','Ice',334,60,62,50,62,60,40,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (460,'Abomasnow','Grass','Ice',494,90,92,75,92,85,60,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (461,'Weavile','Dark','Ice',510,70,120,65,45,85,125,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (462,'Magnezone','Electric','Steel',535,70,70,115,130,90,60,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (463,'Lickilicky','Normal',NULL,515,110,85,95,80,95,50,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (464,'Rhyperior','Ground','Rock',535,115,140,130,55,55,40,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (465,'Tangrowth','Grass',NULL,535,100,100,125,110,50,50,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (466,'Electivire','Electric',NULL,540,75,123,67,95,85,95,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (467,'Magmortar','Fire',NULL,540,75,95,67,125,95,83,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (468,'Togekiss','Fairy','Flying',545,85,50,95,120,115,80,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (469,'Yanmega','Bug','Flying',515,86,76,86,116,56,95,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (470,'Leafeon','Grass',NULL,525,65,110,130,60,65,95,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (471,'Glaceon','Ice',NULL,525,65,60,110,130,95,65,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (472,'Gliscor','Ground','Flying',510,75,95,125,45,75,95,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (473,'Mamoswine','Ice','Ground',530,110,130,80,70,60,80,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (474,'Porygon-Z','Normal',NULL,535,85,80,70,135,75,90,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (475,'Gallade','Psychic','Fighting',518,68,125,65,65,115,80,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (476,'Probopass','Rock','Steel',525,60,55,145,75,150,40,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (477,'Dusknoir','Ghost',NULL,525,45,100,135,65,135,45,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (478,'Froslass','Ice','Ghost',480,70,80,70,80,70,110,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (479,'Rotom','Electric','Ghost',440,50,50,77,95,77,91,4,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (480,'Uxie','Psychic',NULL,580,75,75,130,75,130,95,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (481,'Mesprit','Psychic',NULL,580,80,105,105,105,105,80,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (482,'Azelf','Psychic',NULL,580,75,125,70,125,70,115,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (483,'Dialga','Steel','Dragon',680,100,120,120,150,100,90,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (484,'Palkia','Water','Dragon',680,90,120,100,150,120,100,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (485,'Heatran','Fire','Steel',600,91,90,106,130,106,77,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (486,'Regigigas','Normal',NULL,670,110,160,110,80,110,100,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (487,'GiratinaOrigin Forme','Ghost','Dragon',680,150,120,100,120,100,90,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (488,'Cresselia','Psychic',NULL,600,120,70,120,75,130,85,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (489,'Phione','Water',NULL,480,80,80,80,80,80,80,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (490,'Manaphy','Water',NULL,600,100,100,100,100,100,100,4,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (491,'Darkrai','Dark',NULL,600,70,90,90,135,90,125,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (492,'ShayminLand Forme','Grass',NULL,600,100,100,100,100,100,100,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (493,'Arceus','Normal',NULL,720,120,120,120,120,120,120,4,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (494,'Victini','Psychic','Fire',600,100,100,100,100,100,100,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (495,'Snivy','Grass',NULL,308,45,45,55,45,55,63,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (496,'Servine','Grass',NULL,413,60,60,75,60,75,83,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (497,'Serperior','Grass',NULL,528,75,75,95,75,95,113,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (498,'Tepig','Fire',NULL,308,65,63,45,45,45,45,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (499,'Pignite','Fire','Fighting',418,90,93,55,70,55,55,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (500,'Emboar','Fire','Fighting',528,110,123,65,100,65,65,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (501,'Oshawott','Water',NULL,308,55,55,45,63,45,45,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (502,'Dewott','Water',NULL,413,75,75,60,83,60,60,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (503,'Samurott','Water',NULL,528,95,100,85,108,70,70,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (504,'Patrat','Normal',NULL,255,45,55,39,35,39,42,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (505,'Watchog','Normal',NULL,420,60,85,69,60,69,77,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (506,'Lillipup','Normal',NULL,275,45,60,45,25,45,55,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (507,'Herdier','Normal',NULL,370,65,80,65,35,65,60,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (508,'Stoutland','Normal',NULL,500,85,110,90,45,90,80,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (509,'Purrloin','Dark',NULL,281,41,50,37,50,37,66,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (510,'Liepard','Dark',NULL,446,64,88,50,88,50,106,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (511,'Pansage','Grass',NULL,316,50,53,48,53,48,64,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (512,'Simisage','Grass',NULL,498,75,98,63,98,63,101,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (513,'Pansear','Fire',NULL,316,50,53,48,53,48,64,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (514,'Simisear','Fire',NULL,498,75,98,63,98,63,101,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (515,'Panpour','Water',NULL,316,50,53,48,53,48,64,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (516,'Simipour','Water',NULL,498,75,98,63,98,63,101,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (517,'Munna','Psychic',NULL,292,76,25,45,67,55,24,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (518,'Musharna','Psychic',NULL,487,116,55,85,107,95,29,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (519,'Pidove','Normal','Flying',264,50,55,50,36,30,43,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (520,'Tranquill','Normal','Flying',358,62,77,62,50,42,65,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (521,'Unfezant','Normal','Flying',488,80,115,80,65,55,93,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (522,'Blitzle','Electric',NULL,295,45,60,32,50,32,76,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (523,'Zebstrika','Electric',NULL,497,75,100,63,80,63,116,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (524,'Roggenrola','Rock',NULL,280,55,75,85,25,25,15,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (525,'Boldore','Rock',NULL,390,70,105,105,50,40,20,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (526,'Gigalith','Rock',NULL,515,85,135,130,60,80,25,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (527,'Woobat','Psychic','Flying',313,55,45,43,55,43,72,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (528,'Swoobat','Psychic','Flying',425,67,57,55,77,55,114,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (529,'Drilbur','Ground',NULL,328,60,85,40,30,45,68,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (530,'Excadrill','Ground','Steel',508,110,135,60,50,65,88,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (531,'Audino','Normal',NULL,445,103,60,86,60,86,50,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (532,'Timburr','Fighting',NULL,305,75,80,55,25,35,35,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (533,'Gurdurr','Fighting',NULL,405,85,105,85,40,50,40,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (534,'Conkeldurr','Fighting',NULL,505,105,140,95,55,65,45,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (535,'Tympole','Water',NULL,294,50,50,40,50,40,64,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (536,'Palpitoad','Water','Ground',384,75,65,55,65,55,69,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (537,'Seismitoad','Water','Ground',509,105,95,75,85,75,74,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (538,'Throh','Fighting',NULL,465,120,100,85,30,85,45,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (539,'Sawk','Fighting',NULL,465,75,125,75,30,75,85,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (540,'Sewaddle','Bug','Grass',310,45,53,70,40,60,42,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (541,'Swadloon','Bug','Grass',380,55,63,90,50,80,42,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (542,'Leavanny','Bug','Grass',500,75,103,80,70,80,92,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (543,'Venipede','Bug','Poison',260,30,45,59,30,39,57,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (544,'Whirlipede','Bug','Poison',360,40,55,99,40,79,47,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (545,'Scolipede','Bug','Poison',485,60,100,89,55,69,112,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (546,'Cottonee','Grass','Fairy',280,40,27,60,37,50,66,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (547,'Whimsicott','Grass','Fairy',480,60,67,85,77,75,116,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (548,'Petilil','Grass',NULL,280,45,35,50,70,50,30,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (549,'Lilligant','Grass',NULL,480,70,60,75,110,75,90,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (550,'Basculin','Water',NULL,460,70,92,65,80,55,98,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (551,'Sandile','Ground','Dark',292,50,72,35,35,35,65,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (552,'Krokorok','Ground','Dark',351,60,82,45,45,45,74,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (553,'Krookodile','Ground','Dark',519,95,117,80,65,70,92,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (554,'Darumaka','Fire',NULL,315,70,90,45,15,45,50,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (555,'DarmanitanStandard Mode','Fire',NULL,480,105,140,55,30,55,95,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (556,'Maractus','Grass',NULL,461,75,86,67,106,67,60,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (557,'Dwebble','Bug','Rock',325,50,65,85,35,35,55,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (558,'Crustle','Bug','Rock',475,70,95,125,65,75,45,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (559,'Scraggy','Dark','Fighting',348,50,75,70,35,70,48,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (560,'Scrafty','Dark','Fighting',488,65,90,115,45,115,58,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (561,'Sigilyph','Psychic','Flying',490,72,58,80,103,80,97,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (562,'Yamask','Ghost',NULL,303,38,30,85,55,65,30,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (563,'Cofagrigus','Ghost',NULL,483,58,50,145,95,105,30,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (564,'Tirtouga','Water','Rock',355,54,78,103,53,45,22,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (565,'Carracosta','Water','Rock',495,74,108,133,83,65,32,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (566,'Archen','Rock','Flying',401,55,112,45,74,45,70,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (567,'Archeops','Rock','Flying',567,75,140,65,112,65,110,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (568,'Trubbish','Poison',NULL,329,50,50,62,40,62,65,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (569,'Garbodor','Poison',NULL,474,80,95,82,60,82,75,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (570,'Zorua','Dark',NULL,330,40,65,40,80,40,65,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (571,'Zoroark','Dark',NULL,510,60,105,60,120,60,105,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (572,'Minccino','Normal',NULL,300,55,50,40,40,40,75,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (573,'Cinccino','Normal',NULL,470,75,95,60,65,60,115,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (574,'Gothita','Psychic',NULL,290,45,30,50,55,65,45,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (575,'Gothorita','Psychic',NULL,390,60,45,70,75,85,55,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (576,'Gothitelle','Psychic',NULL,490,70,55,95,95,110,65,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (577,'Solosis','Psychic',NULL,290,45,30,40,105,50,20,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (578,'Duosion','Psychic',NULL,370,65,40,50,125,60,30,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (579,'Reuniclus','Psychic',NULL,490,110,65,75,125,85,30,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (580,'Ducklett','Water','Flying',305,62,44,50,44,50,55,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (581,'Swanna','Water','Flying',473,75,87,63,87,63,98,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (582,'Vanillite','Ice',NULL,305,36,50,50,65,60,44,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (583,'Vanillish','Ice',NULL,395,51,65,65,80,75,59,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (584,'Vanilluxe','Ice',NULL,535,71,95,85,110,95,79,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (585,'Deerling','Normal','Grass',335,60,60,50,40,50,75,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (586,'Sawsbuck','Normal','Grass',475,80,100,70,60,70,95,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (587,'Emolga','Electric','Flying',428,55,75,60,75,60,103,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (588,'Karrablast','Bug',NULL,315,50,75,45,40,45,60,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (589,'Escavalier','Bug','Steel',495,70,135,105,60,105,20,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (590,'Foongus','Grass','Poison',294,69,55,45,55,55,15,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (591,'Amoonguss','Grass','Poison',464,114,85,70,85,80,30,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (592,'Frillish','Water','Ghost',335,55,40,50,65,85,40,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (593,'Jellicent','Water','Ghost',480,100,60,70,85,105,60,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (594,'Alomomola','Water',NULL,470,165,75,80,40,45,65,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (595,'Joltik','Bug','Electric',319,50,47,50,57,50,65,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (596,'Galvantula','Bug','Electric',472,70,77,60,97,60,108,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (597,'Ferroseed','Grass','Steel',305,44,50,91,24,86,10,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (598,'Ferrothorn','Grass','Steel',489,74,94,131,54,116,20,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (599,'Klink','Steel',NULL,300,40,55,70,45,60,30,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (600,'Klang','Steel',NULL,440,60,80,95,70,85,50,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (601,'Klinklang','Steel',NULL,520,60,100,115,70,85,90,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (602,'Tynamo','Electric',NULL,275,35,55,40,45,40,60,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (603,'Eelektrik','Electric',NULL,405,65,85,70,75,70,40,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (604,'Eelektross','Electric',NULL,515,85,115,80,105,80,50,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (605,'Elgyem','Psychic',NULL,335,55,55,55,85,55,30,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (606,'Beheeyem','Psychic',NULL,485,75,75,75,125,95,40,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (607,'Litwick','Ghost','Fire',275,50,30,55,65,55,20,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (608,'Lampent','Ghost','Fire',370,60,40,60,95,60,55,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (609,'Chandelure','Ghost','Fire',520,60,55,90,145,90,80,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (610,'Axew','Dragon',NULL,320,46,87,60,30,40,57,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (611,'Fraxure','Dragon',NULL,410,66,117,70,40,50,67,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (612,'Haxorus','Dragon',NULL,540,76,147,90,60,70,97,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (613,'Cubchoo','Ice',NULL,305,55,70,40,60,40,40,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (614,'Beartic','Ice',NULL,485,95,110,80,70,80,50,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (615,'Cryogonal','Ice',NULL,485,70,50,30,95,135,105,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (616,'Shelmet','Bug',NULL,305,50,40,85,40,65,25,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (617,'Accelgor','Bug',NULL,495,80,70,40,100,60,145,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (618,'Stunfisk','Ground','Electric',471,109,66,84,81,99,32,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (619,'Mienfoo','Fighting',NULL,350,45,85,50,55,50,65,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (620,'Mienshao','Fighting',NULL,510,65,125,60,95,60,105,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (621,'Druddigon','Dragon',NULL,485,77,120,90,60,90,48,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (622,'Golett','Ground','Ghost',303,59,74,50,35,50,35,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (623,'Golurk','Ground','Ghost',483,89,124,80,55,80,55,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (624,'Pawniard','Dark','Steel',340,45,85,70,40,40,60,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (625,'Bisharp','Dark','Steel',490,65,125,100,60,70,70,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (626,'Bouffalant','Normal',NULL,490,95,110,95,40,95,55,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (627,'Rufflet','Normal','Flying',350,70,83,50,37,50,60,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (628,'Braviary','Normal','Flying',510,100,123,75,57,75,80,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (629,'Vullaby','Dark','Flying',370,70,55,75,45,65,60,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (630,'Mandibuzz','Dark','Flying',510,110,65,105,55,95,80,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (631,'Heatmor','Fire',NULL,484,85,97,66,105,66,65,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (632,'Durant','Bug','Steel',484,58,109,112,48,48,109,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (633,'Deino','Dark','Dragon',300,52,65,50,45,50,38,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (634,'Zweilous','Dark','Dragon',420,72,85,70,65,70,58,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (635,'Hydreigon','Dark','Dragon',600,92,105,90,125,90,98,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (636,'Larvesta','Bug','Fire',360,55,85,55,50,55,60,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (637,'Volcarona','Bug','Fire',550,85,60,65,135,105,100,5,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (638,'Cobalion','Steel','Fighting',580,91,90,129,90,72,108,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (639,'Terrakion','Rock','Fighting',580,91,129,90,72,90,108,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (640,'Virizion','Grass','Fighting',580,91,90,72,90,129,108,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (641,'TornadusIncarnate Forme','Flying',NULL,580,79,115,70,125,80,111,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (642,'ThundurusIncarnate Forme','Electric','Flying',580,79,115,70,125,80,111,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (643,'Reshiram','Dragon','Fire',680,100,120,100,150,120,90,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (644,'Zekrom','Dragon','Electric',680,100,150,120,120,100,90,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (645,'LandorusIncarnate Forme','Ground','Flying',600,89,125,90,115,80,101,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (646,'Kyurem','Dragon','Ice',660,125,130,90,130,90,95,5,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (647,'KeldeoOrdinary Forme','Water','Fighting',580,91,72,90,129,90,108,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (648,'MeloettaAria Forme','Normal','Psychic',600,100,77,77,128,128,90,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (649,'Genesect','Bug','Steel',600,71,120,95,120,95,99,5,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (650,'Chespin','Grass',NULL,313,56,61,65,48,45,38,6,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (651,'Quilladin','Grass',NULL,405,61,78,95,56,58,57,6,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (652,'Chesnaught','Grass','Fighting',530,88,107,122,74,75,64,6,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (653,'Fennekin','Fire',NULL,307,40,45,40,62,60,60,6,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (654,'Braixen','Fire',NULL,409,59,59,58,90,70,73,6,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (655,'Delphox','Fire','Psychic',534,75,69,72,114,100,104,6,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (656,'Froakie','Water',NULL,314,41,56,40,62,44,71,6,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (657,'Frogadier','Water',NULL,405,54,63,52,83,56,97,6,'False')"))
   qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (658,'Greninja','Water','Dark',530,72,95,67,103,71,122,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (659,'Bunnelby','Normal',NULL,237,38,36,38,32,36,57,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (660,'Diggersby','Normal','Ground',423,85,56,77,50,77,78,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (661,'Fletchling','Normal','Flying',278,45,50,43,40,38,62,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (662,'Fletchinder','Fire','Flying',382,62,73,55,56,52,84,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (663,'Talonflame','Fire','Flying',499,78,81,71,74,69,126,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (664,'Scatterbug','Bug',NULL,200,38,35,40,27,25,35,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (665,'Spewpa','Bug',NULL,213,45,22,60,27,30,29,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (666,'Vivillon','Bug','Flying',411,80,52,50,90,50,89,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (667,'Litleo','Fire','Normal',369,62,50,58,73,54,72,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (668,'Pyroar','Fire','Normal',507,86,68,72,109,66,106,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (669,'Flabébé','Fairy',NULL,303,44,38,39,61,79,42,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (670,'Floette','Fairy',NULL,371,54,45,47,75,98,52,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (671,'Florges','Fairy',NULL,552,78,65,68,112,154,75,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (672,'Skiddo','Grass',NULL,350,66,65,48,62,57,52,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (673,'Gogoat','Grass',NULL,531,123,100,62,97,81,68,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (674,'Pancham','Fighting',NULL,348,67,82,62,46,48,43,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (675,'Pangoro','Fighting','Dark',495,95,124,78,69,71,58,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (676,'Furfrou','Normal',NULL,472,75,80,60,65,90,102,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (677,'Espurr','Psychic',NULL,355,62,48,54,63,60,68,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (678,'MeowsticMale','Psychic',NULL,466,74,48,76,83,81,104,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (679,'Honedge','Steel','Ghost',325,45,80,100,35,37,28,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (680,'Doublade','Steel','Ghost',448,59,110,150,45,49,35,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (681,'AegislashBlade Forme','Steel','Ghost',520,60,150,50,150,50,60,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (682,'Spritzee','Fairy',NULL,341,78,52,60,63,65,23,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (683,'Aromatisse','Fairy',NULL,462,101,72,72,99,89,29,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (684,'Swirlix','Fairy',NULL,341,62,48,66,59,57,49,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (685,'Slurpuff','Fairy',NULL,480,82,80,86,85,75,72,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (686,'Inkay','Dark','Psychic',288,53,54,53,37,46,45,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (687,'Malamar','Dark','Psychic',482,86,92,88,68,75,73,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (688,'Binacle','Rock','Water',306,42,52,67,39,56,50,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (689,'Barbaracle','Rock','Water',500,72,105,115,54,86,68,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (690,'Skrelp','Poison','Water',320,50,60,60,60,60,30,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (691,'Dragalge','Poison','Dragon',494,65,75,90,97,123,44,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (692,'Clauncher','Water',NULL,330,50,53,62,58,63,44,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (693,'Clawitzer','Water',NULL,500,71,73,88,120,89,59,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (694,'Helioptile','Electric','Normal',289,44,38,33,61,43,70,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (695,'Heliolisk','Electric','Normal',481,62,55,52,109,94,109,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (696,'Tyrunt','Rock','Dragon',362,58,89,77,45,45,48,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (697,'Tyrantrum','Rock','Dragon',521,82,121,119,69,59,71,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (698,'Amaura','Rock','Ice',362,77,59,50,67,63,46,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (699,'Aurorus','Rock','Ice',521,123,77,72,99,92,58,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (700,'Sylveon','Fairy',NULL,525,95,65,65,110,130,60,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (701,'Hawlucha','Fighting','Flying',500,78,92,75,74,63,118,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (702,'Dedenne','Electric','Fairy',431,67,58,57,81,67,101,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (703,'Carbink','Rock','Fairy',500,50,50,150,50,150,50,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (704,'Goomy','Dragon',NULL,300,45,50,35,55,75,40,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (705,'Sliggoo','Dragon',NULL,452,68,75,53,83,113,60,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (706,'Goodra','Dragon',NULL,600,90,100,70,110,150,80,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (707,'Klefki','Steel','Fairy',470,57,80,91,80,87,75,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (708,'Phantump','Ghost','Grass',309,43,70,48,50,60,38,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (709,'Trevenant','Ghost','Grass',474,85,110,76,65,82,56,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (710,'PumpkabooAverage Size','Ghost','Grass',335,49,66,70,44,55,51,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (711,'GourgeistAverage Size','Ghost','Grass',494,65,90,122,58,75,84,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (712,'Bergmite','Ice',NULL,304,55,69,85,32,35,28,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (713,'Avalugg','Ice',NULL,514,95,117,184,44,46,28,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (714,'Noibat','Flying','Dragon',245,40,30,35,45,40,55,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (715,'Noivern','Flying','Dragon',535,85,70,80,97,80,123,6,'False')"))
qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (716,'Xerneas','Fairy',NULL,680,126,131,95,131,98,99,6,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (717,'Yveltal','Dark','Flying',680,126,131,95,131,98,99,6,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (718,'Zygarde50% Forme','Dragon','Ground',600,108,100,121,81,95,95,6,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (719,'Diancie','Rock','Fairy',600,50,100,150,100,150,50,6,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (720,'HoopaHoopa Confined','Psychic','Ghost',600,80,110,60,150,130,70,6,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
if(!queryy.exec("INSERT INTO pokedex2(id,name,type_1,type_2,total,hp,attack,defense,sp_Atk,sp_Def,speed,generation,legendary) VALUES (721,'Volcanion','Fire','Water',600,80,110,120,130,90,70,6,'True')"))
                qWarning() << "MainWindow::DatabasePopulate - ERROR: " << queryy.lastError().text();
}
