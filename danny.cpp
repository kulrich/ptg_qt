#include "danny.h"
#include "ui_danny.h"
#include <QIcon>

danny::danny(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::danny)
{
    ui->setupUi(this);

    QPixmap pixmap("dannygaendon.png");
    QIcon ButtonIcon(pixmap);
    this->ui->dannybutton->setIcon(ButtonIcon);
    this->ui->dannybutton->setIconSize(pixmap.rect().size());
    //this->ui->dannybutton->setStyleSheet("border-image:url(dannygaendon.png);");

    this->ui->label_2->hide();


}

danny::~danny()
{
    delete ui;
}

void danny::on_dannybutton_clicked()
{
    if (this->ui->label->isVisible())
    {
        this->ui->label->hide();
        this->ui->label_2->show();
    }
    else
    {
        this->ui->label_2->hide();
        this->ui->label->show();
    }
    dannyCounter++;
    if (dannyCounter >= 6)
    {
        this->close();
    }
}
