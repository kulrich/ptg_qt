#include "pkmninfo.h"
#include "ui_pkmninfo.h"

pkmnInfo::pkmnInfo(QWidget *parent, int id) :
    QDialog(parent),
    ui(new Ui::pkmnInfo)
{
    ui->setupUi(this);
    //ui->label->setText(QString::number(id) + ", das penispokemon");
    m_id = id;
//id INTEGER PRIMARY KEY ,name TEXT, type_1 TEXT, type_2 TEXT, total INTEGER, hp INTEGER, attack INTEGER, defense INTEGER, sp_Atk INTEGER, sp_Def INTEGER, speed INTEGER, generation INTEGER, legendary TEXT)");

    QString id_text = "#" + QString::number(m_id);
    this->ui->id->setText(id_text);
    this->ui->name->setText(execQueryGer("name"));
    this->ui->typ1->setText(execQueryGer("type1"));
    this->ui->typ2->setText(execQueryGer("type2"));
    this->ui->atk->setText(execQuery("attack"));
    this->ui->sp_atk->setText(execQuery("sp_Atk"));
    this->ui->def->setText(execQuery("defense"));
    this->ui->sp_def->setText(execQuery("sp_Def"));
    this->ui->init->setText(execQuery("speed"));
    this->ui->hp->setText(execQuery("hp"));
    this->ui->ges->setText(execQuery("total"));

}

pkmnInfo::~pkmnInfo()
{
    delete ui;
}

QString pkmnInfo::execQuery(QString attr)
{
    QString queryText = "SELECT " + attr + " FROM pokedex2 WHERE ID = ?";

    query.prepare(queryText);
    query.addBindValue(m_id);

    if(!query.exec())
    {
        qWarning() << "MainWindow::OnSearchClicked - ERROR: " << query.lastError().text();
    }

    if(query.first())
    {
        return query.value(0).toString();
    }
    return attr;
}

QString pkmnInfo::execQueryGer(QString attr)
{
    QString queryText = "SELECT " + attr + " FROM pokedex WHERE ID = ?";

    query.prepare(queryText);
    query.addBindValue(m_id);

    if(!query.exec())
    {
        qWarning() << "MainWindow::OnSearchClicked - ERROR: " << query.lastError().text();
    }

    if(query.first())
    {
        return query.value(0).toString();
    }
    return attr;
}
