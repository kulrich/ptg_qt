#ifndef DANNY_H
#define DANNY_H

#include <QDialog>

namespace Ui {
class danny;
}

class danny : public QDialog
{
    Q_OBJECT

public:
    explicit danny(QWidget *parent = nullptr);
    ~danny();

private slots:
    void on_dannybutton_clicked();

private:
    Ui::danny *ui;
    int dannyCounter = 0;
};

#endif // DANNY_H
