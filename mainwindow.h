#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QLabel>
#include <QCheckBox>
#include <QListWidget>
#include <QPushButton>
#include <QApplication>
#include <iostream>
#include <QtSql>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <QString>
#include <random>
#include <QDateTime>
#include <cstdlib>
#include <ctime>
#include <QDebug>
#include <QHBoxLayout>
#include <QIntValidator>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <thread>
#include <chrono>
#include "database.h"
#include <QDialog>
#include "danny.h"
#include <QFile>
#include <QDir>
#include "pkmninfo.h"
#include <QTableWidgetItem>
#include <QTableWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    /**
     * @brief Triggers the random team generation for team 1.
     */
    void on_generate1_clicked();

    /**
     * @brief Triggers the random team generation for team 2.
     */
    void on_generate2_clicked();

    /**
     * @brief Triggers the random team generation for team 3.
     */
    void on_generate3_clicked();

    /**
     * @brief Triggers the random team generation for team 4.
     */
    void on_generate4_clicked();

    /**
     * @brief Triggers the random team generation for team 5.
     */
    void on_generate5_clicked();

    /**
     * @brief Triggers the random team generation for team 6.
     */
    void on_generate6_clicked();

    /**
     * @brief Triggers the random team generation for team 7.
     */
    void on_generate7_clicked();

    /**
     * @brief Triggers the random team generation for team 8.
     */
    void on_generate8_clicked();

    /**
     * @brief Triggers the random team generation for all teams.
     */
    void on_generateAll_clicked();

    /**
     * @brief Fills the team with random pokémon. Creates an array and fills the
     *        anzTeam elements of the array with random numbers depending on the
     *        size of the pokemon list.
     * @param anzTeam numbers of pokémon in the team
     * @param teamNr team ID
     */
    void fillTeam(int anzTeam, int teamNr);

    /**
     * @brief calls 'addByGen' or 'removeByGen' with parameter '1' whether the
     * button is checked or not.
     */
    void on_gen1_stateChanged();

    /**
     * @brief calls 'addByGen' or 'removeByGen' with parameter '2' whether the
     * button is checked or not.
     */
    void on_gen2_stateChanged();

    /**
     * @brief calls 'addByGen' or 'removeByGen' with parameter '3' whether the
     * button is checked or not.
     */
    void on_gen3_stateChanged();

    /**
     * @brief calls 'addByGen' or 'removeByGen' with parameter '4' whether the
     * button is checked or not.
     */
    void on_gen4_stateChanged();

    /**
     * @brief calls 'addByGen' or 'removeByGen' with parameter '5' whether the
     * button is checked or not.
     */
    void on_gen5_stateChanged();

    /**
     * @brief calls 'addByGen' or 'removeByGen' with parameter '6' whether the
     * button is checked or not.
     */
    void on_gen6_stateChanged();

    /**
     * @brief calls 'addByGen' or 'removeByGen' with parameter '7' whether the
     * button is checked or not.
     */
    void on_gen7_stateChanged();

    /**
     * @brief calls 'addByGen' or 'removeByGen' with parameter '8' whether the
     * button is checked or not.
     */
    void on_gen8_stateChanged();

    /**
     * @brief sets the number of pokémon in team 1 by index
     * @param index the number of pokémon to set
     */
    void on_anzPkmn1_currentIndexChanged(int index);

    /**
     * @brief sets the number of pokémon in team 2 by index
     * @param index the number of pokémon to set
     */
    void on_anzPkmn2_currentIndexChanged(int index);

    /**
     * @brief sets the number of pokémon in team 3 by index
     * @param index the number of pokémon to set
     */
    void on_anzPkmn3_currentIndexChanged(int index);

    /**
     * @brief sets the number of pokémon in team 4 by index
     * @param index the number of pokémon to set
     */
    void on_anzPkmn4_currentIndexChanged(int index);

    /**
     * @brief sets the number of pokémon in team 5 by index
     * @param index the number of pokémon to set
     */
    void on_anzPkmn5_currentIndexChanged(int index);

    /**
     * @brief sets the number of pokémon in team 6 by index
     * @param index the number of pokémon to set
     */
    void on_anzPkmn6_currentIndexChanged(int index);

    /**
     * @brief sets the number of pokémon in team 7 by index
     * @param index the number of pokémon to set
     */
    void on_anzPkmn7_currentIndexChanged(int index);

    /**
     * @brief sets the number of pokémon in team 8 by index
     * @param index the number of pokémon to set
     */
    void on_anzPkmn8_currentIndexChanged(int index);

    /**
     * @brief gets a random number with DateTime from Qt limited by current
     *        number of pokémon
     * @return random number
     */
    int getRandom();

    /**
     * @brief function to get the name of the pokémon by given index
     * @param id The pokémon id to get the name of
     * @return the name of the pokémon
     */
    QString getName(int id);

    /**
     * @brief function to get the ID of the pokémon by given name
     * @param name The pokémon name to get the n ame of
     * @return the name of the pokémon
     */
    int getId(QString name);


    void specialListAdd(int gen, int legi, QList<int>& list);
    void phaseListAdd(int gen, int phase, QList<int>& list);
    void typeListAdd(int gen, QString type, QList<int>& list, int whichType);

    void on_check_normal_stateChanged();
    void on_check_legi_stateChanged();
    void on_check_mystery_stateChanged();
    void on_check_ultra_stateChanged();
    void on_check_baby_stateChanged();
    void on_check_base_stateChanged();
    void on_check_phase1_stateChanged();
    void on_check_phase2_stateChanged();
    void refreshPkmnList(QCheckBox* checkBox, QList<int>& list, bool isGenChecked);
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();
    void addTeamSize(QComboBox* anzPkmn);

    void on_addTeam_clicked();

    void on_rmTeam_clicked();

    void genAdd(QCheckBox* box, QList<int> list);
    void genRemove(QList<int> list);
    void removeByGen(int gen);
    void addByGen(int gen);
    void helpFktTypes(QList<int> typen[], int gen, int whichType);


    void on_feuer1_stateChanged();
    void on_feuer2_stateChanged();
    void on_pflanze1_stateChanged();
    void on_pflanze2_stateChanged();
    void on_wasser1_stateChanged();
    void on_wasser2_stateChanged();
    void on_normal1_stateChanged();
    void on_normal2_stateChanged();
    void on_boden1_stateChanged();
    void on_boden2_stateChanged();
    void on_gestein1_stateChanged();
    void on_gestein2_stateChanged();
    void on_elektro1_stateChanged();
    void on_elektro2_stateChanged();
    void on_kaefer1_stateChanged();
    void on_kaefer2_stateChanged();
    void on_geist1_stateChanged();
    void on_geist2_stateChanged();
    void on_stahl1_stateChanged();
    void on_stahl2_stateChanged();
    void on_eis1_stateChanged();
    void on_eis2_stateChanged();
    void on_drache1_stateChanged();
    void on_drache2_stateChanged();
    void on_fee1_stateChanged();
    void on_fee2_stateChanged();
    void on_kampf1_stateChanged();
    void on_kampf2_stateChanged();
    void on_flug1_stateChanged();
    void on_flug2_stateChanged();
    void on_gift1_stateChanged();
    void on_gift2_stateChanged();
    void on_psycho1_stateChanged();
    void on_psycho2_stateChanged();
    void on_unlicht1_stateChanged();
    void on_unlicht2_stateChanged();
    void sortlist();
    void refreshListWidget(QListWidget* listw, QList<int> list);
    void on_removeButton_clicked();

    void on_restore_clicked();

    void on_addButton_clicked();
    void on_save_clicked();
    void on_load_clicked();

    void on_checkAll_clicked();
    void on_checkAll_2_clicked();
    void on_uncheckAll_clicked();
    void on_uncheckAll_2_clicked();

    void on_pushButton_3_clicked();

    void openPkmnInfo (QString text);

    void on_team1Widget_itemDoubleClicked(QListWidgetItem *item);
    void on_team2Widget_itemDoubleClicked(QListWidgetItem *item);
    void on_team3Widget_itemDoubleClicked(QListWidgetItem *item);
    void on_team4Widget_itemDoubleClicked(QListWidgetItem *item);
    void on_team5Widget_itemDoubleClicked(QListWidgetItem *item);
    void on_team6Widget_itemDoubleClicked(QListWidgetItem *item);
    void on_team7Widget_itemDoubleClicked(QListWidgetItem *item);
    void on_team8Widget_itemDoubleClicked(QListWidgetItem *item);

    void on_search_textEdited(const QString &arg1);


private:
    Ui::MainWindow *ui;
    bool clicked = false;
    bool gen1 = true;
    QList<int> pkmnlist;
    QList<int> pkmnlist_backup;
    QList<int> removedlist;
    QList<int> normals[9];
    QList<int> legis[9];
    QList<int> mysteries[9];
    QList<int> ultras;
    QList<int> babies[9];
    QList<int> bases[9];
    QList<int> phase1[9];
    QList<int> phase2[9];
    QList<int> typen1[9][18];
    QList<int> typen2[9][18];
    QList<QCheckBox*> allCheckBoxes[6];
    QList<QPushButton*> allTeamButtons[8];
    QListWidget* allTeamWidgets[8];
    int anzahl;
    int anzTeams = 1;
    int anzTeamX[9];
    int anzCheckboxes = 0;
    int eg = 0;
    int g = 0;
    bool isGenChecked[9] = {false, false, false, false, false, false, false, false, false};
};

#endif // MAINWINDOW_H
