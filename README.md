# Pokémon Team Generator

The Pokémon Team Generator (PTG) is a tool for generating randomized Pokémon teams. You can customize which pokémon can be chosen. You can e.g. get a random team out of all base fire pokémon from generations 1, 2 and 5 or a team out of all phase 1 pokémon with ice as secondary type from generation 1 and 7. The tool is still WIP, so bugs will be fixed, features will be improved and new features will be added.

## Features

* Randomize up to 8 teams with up to 6 Pokémon, seperately or all by one click
* The randomizer knows the first 809 Pokémon (generation 1 - 7)
* Choose from which generation(s) the Pokémon will be chosen
* Choose the first and second type(s)
* Choose between base, first evolution and/or second evolution
* Choose between non-legendary, legendary, mysterious and/or ultra beasts
* Save or load your configuration
* Customize the Pokémon list by adding and removing single Pokémon from the list (with search function)
* Pokémon information by double-clicking on a pokémon in a randomized team

## Planned features
* Balanced teams (by stats)
* Saving teams like configurations
* More languages
* Improved Pokémon information (pictures and more information).
* .... and more!

### Installing

This is just a QT project (for now). To run PTG you have to open this project in "QT creator", build and run it. Tested on Windows and Linux (Ubuntu). 

## Information

This tool is basically a list which is edited by the customizations. It uses an internally database but there are plans to use an online database.

## Authors

* **Kevin Ulrich** - *Initial work* - [kulrich](https://github.com/kulrich)

