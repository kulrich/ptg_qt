#ifndef PKMNINFO_H
#define PKMNINFO_H

#include <QDialog>
#include <QString>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>

namespace Ui {
class pkmnInfo;
}

class pkmnInfo : public QDialog
{
    Q_OBJECT

public:
    explicit pkmnInfo(QWidget *parent = nullptr, int id = 0);

    QString execQuery(QString attr);
    QString execQueryGer(QString attr);
    ~pkmnInfo();

private:
    Ui::pkmnInfo *ui;
    QSqlQuery query;
    int m_id;
};

#endif // PKMNINFO_H
