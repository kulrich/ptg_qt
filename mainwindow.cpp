#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    srand(static_cast<unsigned int>(time(nullptr)));

    // -- DATABASE INIT --
    /*DatabaseConnect();
    DatabaseInit();
    DatabasePopulate();*/
    Database db;

    QDir directory("states");
    QStringList states = directory.entryList(QStringList() << "*.ptg" << "*.PTG",QDir::Files);
    foreach(QString filename, states) {
    //do whatever you need to do
        QRegExp rx("(\\.)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
        QStringList query = filename.split(rx);
        this->ui->statesList->addItem(query.first());
    }


    this->ui->tableWidget->insertRow(2);
    this->ui->tableWidget->insertColumn(2);

    QTableWidgetItem* tableItem1 = new QTableWidgetItem;
    tableItem1->setText("dannyGaendon");
    this->ui->tableWidget->setItem(1,1,tableItem1);
    this->ui->tableWidget->show();



    for (int i = 1; i <= 7; i++)
    {
        specialListAdd(i, 2, mysteries[i]);
        specialListAdd(i, 0, normals[i]);
        if (i == 7)
        {
            specialListAdd(i, 3, ultras);
        }
        specialListAdd(i, 1, legis[i]);
        if (i == 2 || i == 3 || i == 4)
        {
            phaseListAdd(i, 3, babies[i]);
        }
        phaseListAdd(i, 0, bases[i]);
        phaseListAdd(i, 1, phase1[i]);
        phaseListAdd(i, 2, phase2[i]);
        helpFktTypes(typen1[i], i, 1);
        helpFktTypes(typen2[i], i, 2);
    }

    // Set Gen-Boxes checked
    this->ui->gen8->setEnabled(false);

    allCheckBoxes[0] = ui->genBoxes->findChildren<QCheckBox*>();
    allCheckBoxes[1] = ui->typeBoxes->findChildren<QCheckBox*>();
    allCheckBoxes[2] = ui->type1Boxes->findChildren<QCheckBox*>();
    allCheckBoxes[3] = ui->type2Boxes->findChildren<QCheckBox*>();
    allCheckBoxes[4] = ui->phaseBoxes->findChildren<QCheckBox*>();
    allCheckBoxes[5] = ui->miscBoxes->findChildren<QCheckBox*>();

    for (int i = 0; i < 6; i++)
    {
        foreach (QCheckBox *cb,allCheckBoxes[i])
        {
            anzCheckboxes++;
            cb->setChecked(true);
        }
    }
    qDebug() << anzCheckboxes;



    allTeamWidgets[0] = ui->team1Widget;
    allTeamWidgets[1] = ui->team2Widget;
    allTeamWidgets[2] = ui->team3Widget;
    allTeamWidgets[3] = ui->team4Widget;
    allTeamWidgets[4] = ui->team5Widget;
    allTeamWidgets[5] = ui->team6Widget;
    allTeamWidgets[6] = ui->team7Widget;
    allTeamWidgets[7] = ui->team8Widget;


    /*for (int i = 0; i < 8; i++)
    {
        foreach (QPushButton *pb,allTeamButtons[i])
        {
            //anzCheckboxes++;
            pb->setText("");
            pb->setEnabled(false);
        }
    }*/


    // Add team size options for dropdown menu
    addTeamSize(this->ui->anzPkmn1);
    addTeamSize(this->ui->anzPkmn2);
    addTeamSize(this->ui->anzPkmn3);
    addTeamSize(this->ui->anzPkmn4);
    addTeamSize(this->ui->anzPkmn5);
    addTeamSize(this->ui->anzPkmn6);
    addTeamSize(this->ui->anzPkmn7);
    addTeamSize(this->ui->anzPkmn8);

    // Hide Teams
    this->ui->team2->hide();
    this->ui->team3->hide();
    this->ui->team4->hide();
    this->ui->team5->hide();
    this->ui->team6->hide();
    this->ui->team7->hide();
    this->ui->team8->hide();

    this->ui->generate2->hide();
    this->ui->generate3->hide();
    this->ui->generate4->hide();
    this->ui->generate5->hide();
    this->ui->generate6->hide();
    this->ui->generate7->hide();
    this->ui->generate8->hide();

    //Printe Teamanzahl
    this->ui->label_anzTeams->setText(QString::number(anzTeams));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addTeamSize(QComboBox* anzPkmn)
{
    anzPkmn->addItem("6");
    anzPkmn->addItem("5");
    anzPkmn->addItem("4");
    anzPkmn->addItem("3");
    anzPkmn->addItem("2");
    anzPkmn->addItem("1");
}

void MainWindow::on_generate1_clicked()
{
    //fillTeam(anzTeamX[1], this->ui->pkmnlist1);
    fillTeam(anzTeamX[1], 1);
}

void MainWindow::on_generate2_clicked()
{
    //fillTeam(anzTeamX[2], this->ui->pkmnlist2);
    fillTeam(anzTeamX[2], 2);
}

void MainWindow::on_generate3_clicked()
{
    //fillTeam(anzTeamX[3], this->ui->pkmnlist3);
    fillTeam(anzTeamX[3], 3);
}

void MainWindow::on_generate4_clicked()
{
    //fillTeam(anzTeamX[4], this->ui->pkmnlist4);
    fillTeam(anzTeamX[4], 4);
}

void MainWindow::on_generate5_clicked()
{
    //fillTeam(anzTeamX[5], this->ui->pkmnlist5);
    fillTeam(anzTeamX[5], 5);
}

void MainWindow::on_generate6_clicked()
{
    //fillTeam(anzTeamX[6], this->ui->pkmnlist6);
    fillTeam(anzTeamX[6], 6);
}

void MainWindow::on_generate7_clicked()
{
    //fillTeam(anzTeamX[7], this->ui->pkmnlist7);
    fillTeam(anzTeamX[7], 7);
}

void MainWindow::on_generate8_clicked()
{
    //fillTeam(anzTeamX[8], this->ui->pkmnlist8);
    fillTeam(anzTeamX[8], 8);

}

void MainWindow::fillTeam(int anzTeam, int teamNr)
{
    QString pkmn = "";
    if (pkmnlist.count() >= anzTeam || this->ui->check_multiples->isChecked())
    {
        int random;
        int currentTeam[6] = {0, 0, 0, 0, 0, 0};
        bool ok = false;
        for (int i = 0; i < 6; i++)
        {
            allTeamWidgets[teamNr-1]->clear();
        }
        for(int i = 0; i < anzTeam; i++)
        {

            // pruefen ob merhfache gesetzt, falls nicht: sicherstellen dass keine mehrfachen ins team kommen
            if (!this->ui->check_multiples->isChecked())
            {
                do
                {
                    random = pkmnlist.value(rand()%anzahl);
                    for (int j = 0; j < i; j++)
                    {
                        if (currentTeam[j] == random)
                        {
                            ok = false;
                            break;
                        }
                        else
                        {
                            ok = true;
                        }
                    }
                    if ( i == 0)
                    {
                        ok = true;
                    }
                }
                while(!ok);
                currentTeam[i] = random;
            }
            else
            {
                random = pkmnlist.value(rand()%anzahl);
            }

            QString number = QString("%1").arg(random, 3, 10, QChar('0'));
            pkmn = number + ": " + getName(QString::number(random).toInt());
            //allTeamButtons[teamNr-1].value(i)->setText(pkmn);
            //allTeamButtons[teamNr-1].value(i)->setEnabled(true);
            allTeamWidgets[teamNr-1]->addItem(pkmn);



        }
        //pkmnlistX->setText(pkmn);
    }
    else
    {
        qDebug() << "nicht genug pokemon in der liste";
    }
}

void MainWindow::on_generateAll_clicked()
{
    fillTeam(anzTeamX[1], 1);
    fillTeam(anzTeamX[2], 2);
    fillTeam(anzTeamX[3], 3);
    fillTeam(anzTeamX[4], 4);
    fillTeam(anzTeamX[5], 5);
    fillTeam(anzTeamX[6], 6);
    fillTeam(anzTeamX[7], 7);
    fillTeam(anzTeamX[8], 8);
}

void MainWindow::genAdd(QCheckBox* box, QList<int> list)
{
    if (box->isChecked())
    {
        for (int i = 0; i <= list.count(); i++)
        {
            if (pkmnlist.indexOf(list.value(i)) == -1 && list.value(i) != 0)
            {
                pkmnlist.append(list.value(i));
                pkmnlist_backup.append(list.value(i));
            }
        }
    }
    else
    {
        //qDebug() << "Box nicht gesetzt! Entsprechende Pokemon werden nicht hinzugefuegt.";
    }

    anzahl = pkmnlist.count();
    this->ui->label_anzahl->setText(QString::number(anzahl));

    sortlist();
}

void MainWindow::genRemove(QList<int> list)
{
    for (int i = 0; i <= list.count(); i++)
    {
        if (pkmnlist.indexOf(list.value(i)) != -1 && list.value(i) != 0)
        {
            pkmnlist.removeAll(list.value(i));
            pkmnlist_backup.removeAll(list.value(i));
            ui->listWidget->model()->removeRow(list.value(i)); // remove each row
        }
    }
    anzahl = pkmnlist.count();
    this->ui->label_anzahl->setText(QString::number(anzahl));
}

void MainWindow::addByGen(int gen)
{
    genAdd(this->ui->check_baby, babies[gen]);
    genAdd(this->ui->check_base, bases[gen]);
    genAdd(this->ui->check_phase1, phase1[gen]);
    genAdd(this->ui->check_phase2, phase2[gen]);
    genAdd(this->ui->check_legi, legis[gen]);
    genAdd(this->ui->check_mystery, mysteries[gen]);

    qDebug() << this->ui->check_normal->isChecked();
    genAdd(this->ui->check_normal, normals[gen]);
    genAdd(this->ui->feuer1, typen1[gen][0]);
    refreshListWidget(this->ui->listWidget, pkmnlist);
}

void MainWindow::removeByGen(int gen)
{
    genRemove(babies[gen]);
    genRemove(bases[gen]);
    genRemove(phase1[gen]);
    genRemove(phase2[gen]);
    genRemove(legis[gen]);
    genRemove(mysteries[gen]);
    genRemove(normals[gen]);
    genRemove(typen1[gen][0]);
    refreshListWidget(this->ui->listWidget, pkmnlist);
}

void MainWindow::on_gen1_stateChanged()
{
    isGenChecked[1] = this->ui->gen1->isChecked();
    if (isGenChecked[1])
    {
        addByGen(1);
    }
    else
    {
        removeByGen(1);
    }
}

void MainWindow::on_gen2_stateChanged()
{
    isGenChecked[2] = this->ui->gen2->isChecked();
    if (isGenChecked[2])
    {
        addByGen(2);
    }
    else
    {
        removeByGen(2);
    }
}

void MainWindow::on_gen3_stateChanged()
{
    isGenChecked[3] = this->ui->gen3->isChecked();
    if (isGenChecked[3])
    {
        addByGen(3);
    }
    else
    {
        removeByGen(3);
    }
}

void MainWindow::on_gen4_stateChanged()
{
    isGenChecked[4] = this->ui->gen4->isChecked();
    if (isGenChecked[4])
    {
        addByGen(4);
    }
    else
    {
        removeByGen(4);
    }
}

void MainWindow::on_gen5_stateChanged()
{
    isGenChecked[5] = this->ui->gen5->isChecked();
    if (isGenChecked[5])
    {
        addByGen(5);
    }
    else
    {
        removeByGen(5);
    }
}

void MainWindow::on_gen6_stateChanged()
{
    isGenChecked[6] = this->ui->gen6->isChecked();
    if (isGenChecked[6])
    {
        addByGen(6);
    }
    else
    {
        removeByGen(6);
    }
}

void MainWindow::on_gen7_stateChanged()
{
    isGenChecked[7] = this->ui->gen7->isChecked();
    if (isGenChecked[7])
    {
        addByGen(7);
        genAdd(this->ui->check_ultra, ultras);
    }
    else
    {
        removeByGen(7);
        genRemove(ultras);
    }
}

void MainWindow::on_gen8_stateChanged()
{
/*
    isGenChecked[8] = this->ui->gen8->isChecked();
    if (isGenChecked[8])
    {
        addByGen(8);
    }
    else
    {
        removeByGen(8);
    }
*/
}

void MainWindow::on_anzPkmn1_currentIndexChanged(int index)
{
    anzTeamX[1] = index;
    anzTeamX[1] = this->ui->anzPkmn1->currentText().toInt();
}
void MainWindow::on_anzPkmn2_currentIndexChanged(int index)
{
    anzTeamX[2] = index;
    anzTeamX[2] = this->ui->anzPkmn2->currentText().toInt();
}
void MainWindow::on_anzPkmn3_currentIndexChanged(int index)
{
    anzTeamX[3] = index;
    anzTeamX[3] = this->ui->anzPkmn3->currentText().toInt();
}
void MainWindow::on_anzPkmn4_currentIndexChanged(int index)
{
    anzTeamX[4] = index;
    anzTeamX[4] = this->ui->anzPkmn4->currentText().toInt();
}
void MainWindow::on_anzPkmn5_currentIndexChanged(int index)
{
    anzTeamX[5] = index;
    anzTeamX[5] = this->ui->anzPkmn5->currentText().toInt();
}
void MainWindow::on_anzPkmn6_currentIndexChanged(int index)
{
    anzTeamX[6] = index;
    anzTeamX[6] = this->ui->anzPkmn6->currentText().toInt();
}
void MainWindow::on_anzPkmn7_currentIndexChanged(int index)
{
    anzTeamX[7] = index;
    anzTeamX[7] = this->ui->anzPkmn7->currentText().toInt();
}
void MainWindow::on_anzPkmn8_currentIndexChanged(int index)
{
    anzTeamX[8] = index;
    anzTeamX[8] = this->ui->anzPkmn8->currentText().toInt();
}

int MainWindow::getRandom()
{
    unsigned int ms = static_cast<unsigned>(QDateTime::currentMSecsSinceEpoch());
    std::mt19937 gen(ms);
    std::uniform_int_distribution<> uid(0, anzahl);
    return uid(gen);
}

QString MainWindow::getName(int id)
{
        QSqlQuery query;
        query.prepare("SELECT name FROM pokedex WHERE id = ?");
        query.addBindValue(id);

        if(!query.exec())
            qWarning() << "MainWindow::OnSearchClicked - ERROR: " << query.lastError().text();

        if(query.first())
            //mOutputText->setText(query.value(0).toString());
            return query.value(0).toString();
        else
        {
            return "!!!Error!!!";
        }
}

int MainWindow::getId(QString name)
{
        QSqlQuery query;
        query.prepare("SELECT id FROM pokedex WHERE name = ?");
        query.addBindValue(name);

        if(!query.exec())
            qWarning() << "MainWindow::OnSearchClicked - ERROR: " << query.lastError().text();

        if(query.first())
            //mOutputText->setText(query.value(0).toString());
            return query.value(0).toInt();
        else
        {
            return -1;
        }
}


void MainWindow::specialListAdd(int gen, int legi, QList<int>& list)
{
    QSqlQuery query;
    query.prepare("SELECT id FROM pokedex WHERE legi = ? AND gen = ?");
    query.addBindValue(legi);
    query.addBindValue(gen);

    if(!query.exec())
    {
        qWarning() << "MainWindow::OnSearchClicked - ERROR: " << query.lastError().text();
    }
    if(query.first())
    {
        list.append(query.value(0).toInt());
        while(query.next())
        {
            list.append(query.value(0).toInt());
        }
    }
    else
    {
        qDebug() << "!!!Error bei specialListAdd!!! " << gen << " special: " << legi;

    }
}

void MainWindow::phaseListAdd(int gen, int phase, QList<int>& list)
{
    QSqlQuery query;
    query.prepare("SELECT id FROM pokedex WHERE phase = ? AND gen = ?");
    query.addBindValue(phase);
    query.addBindValue(gen);

    if(!query.exec())
    {
        qWarning() << "MainWindow::OnSearchClicked - ERROR: " << query.lastError().text();
    }
    if(query.first())
    {
        list.append(query.value(0).toInt());
        while(query.next())
        {
            list.append(query.value(0).toInt());
        }
    }
    else
    {
        qDebug() << "!!!Error bei phaseListAdd!!! " << gen << " phase: " << phase;

    }
}

void MainWindow::typeListAdd(int gen, QString type, QList<int>& list, int whichType)
{
    QSqlQuery query;
    if (whichType == 1)
    {
        query.prepare("SELECT id FROM pokedex WHERE type1 = ? AND gen = ?");
    }
    if (whichType == 2)
    {
        query.prepare("SELECT id FROM pokedex WHERE type2 = ? AND gen = ?");
    }
    query.addBindValue(type);
    query.addBindValue(gen);

    if(!query.exec())
    {
        qWarning() << "MainWindow::OnSearchClicked - ERROR: " << query.lastError().text();
    }
    if(query.first())
    {
        list.append(query.value(0).toInt());
        while(query.next())
        {
            list.append(query.value(0).toInt());
        }
    }
}

void MainWindow::on_check_legi_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->check_legi, legis[i], isGenChecked[i]);
    }
}

void MainWindow::on_check_normal_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->check_normal, normals[i], isGenChecked[i]);
    }
}

void MainWindow::on_check_ultra_stateChanged()
{
    refreshPkmnList(this->ui->check_ultra, ultras, isGenChecked[7]);
}

void MainWindow::on_check_mystery_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->check_mystery, mysteries[i], isGenChecked[i]);
    }
}

void MainWindow::on_check_baby_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->check_baby, babies[i], isGenChecked[i]);
    }
}

void MainWindow::on_check_base_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->check_base, bases[i], isGenChecked[i]);
    }
}

void MainWindow::on_check_phase1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->check_phase1, phase1[i], isGenChecked[i]);
    }
}

void MainWindow::on_check_phase2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->check_phase2, phase2[i], isGenChecked[i]);
    }
}

void MainWindow::refreshPkmnList(QCheckBox* checkBox, QList<int>& list, bool isGenChecked)
{
    if (checkBox->isChecked())
    {
        if (isGenChecked)
        {
            for (int i = 0; i <= list.count(); i++)
            {
                if (pkmnlist.indexOf(list.value(i)) == -1 && list.value(i) != 0)
                {
                    pkmnlist.append(list.value(i));
                    pkmnlist_backup.append(list.value(i));
                }
            }
        }
    }
    else
    {
        for (int i = 0; i <= list.count(); i++)
        {
            if (pkmnlist.indexOf(list.value(i)) != -1 && list.value(i) != 0)
            {
                pkmnlist.removeAll(list.value(i));
                pkmnlist_backup.removeAll(list.value(i));
            }
        }
    }

    anzahl = pkmnlist.count();
    this->ui->label_anzahl->setText(QString::number(anzahl));

    sortlist();
    refreshListWidget(this->ui->listWidget, pkmnlist);
}

void MainWindow::on_pushButton_clicked()
{
    this->ui->stackedWidget->setCurrentIndex(1);
    eg++;
    if (eg == 10)
    {
        eg = 0;
        danny d(this);
        d.setModal(true);
        d.exec();
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    this->ui->stackedWidget->setCurrentIndex(0);
    eg++;
    if (eg == 10)
    {
        eg = 0;
        danny d(this);
        d.setModal(true);
        d.exec();
    }
}

void MainWindow::on_addTeam_clicked()
{
    if (anzTeams < 8)
    {
        anzTeams++;
    }
    switch(anzTeams)
    {
        case 8: this->ui->team8->show();
                this->ui->generate8->show();
                break;
        case 7: this->ui->team7->show();
                this->ui->generate7->show();
                break;
        case 6: this->ui->team6->show();
                this->ui->generate6->show();
                break;
        case 5: this->ui->team5->show();
                this->ui->generate5->show();
                break;
        case 4: this->ui->team4->show();
                this->ui->generate4->show();
                break;
        case 3: this->ui->team3->show();
                this->ui->generate3->show();
                break;
        case 2: this->ui->team2->show();
                this->ui->generate2->show();
                break;
        case 1: this->ui->team1->show();
                this->ui->generate1->show();
                break;


    default: break;
    }
    if (anzTeams > 0)
    {
        this->ui->rmTeam->setEnabled(true);
    }
    if (anzTeams >= 8)
    {
        this->ui->addTeam->setEnabled(false);
    }
    this->ui->label_anzTeams->setText(QString::number(anzTeams));
}

void MainWindow::on_rmTeam_clicked()
{
    if (anzTeams > 0)
    {
        anzTeams--;
    }
    switch(anzTeams)
    {
        case 7: this->ui->team8->hide();
                this->ui->generate8->hide();
                break;
        case 6: this->ui->team7->hide();
                this->ui->generate7->hide();
                break;
        case 5: this->ui->team6->hide();
                this->ui->generate6->hide();
                break;
        case 4: this->ui->team5->hide();
                this->ui->generate5->hide();
                break;
        case 3: this->ui->team4->hide();
                this->ui->generate4->hide();
                break;
        case 2: this->ui->team3->hide();
                this->ui->generate3->hide();
                break;
        case 1: this->ui->team2->hide();
                this->ui->generate2->hide();
                break;
        case 0: this->ui->team1->hide();
                this->ui->generate1->hide();
                break;
        default: break;
    }
    if (anzTeams <= 0)
    {
        this->ui->rmTeam->setEnabled(false);
    }
    if (anzTeams < 8)
    {
        this->ui->addTeam->setEnabled(true);
    }
    this->ui->label_anzTeams->setText(QString::number(anzTeams));
}

void MainWindow::helpFktTypes(QList<int> typen[], int gen, int whichType)
{
    typeListAdd(gen, "feuer", typen[0], whichType);
    typeListAdd(gen, "wasser", typen[1], whichType);
    typeListAdd(gen, "pflanze", typen[2], whichType);
    typeListAdd(gen, "elektro", typen[3], whichType);
    typeListAdd(gen, "normal", typen[4], whichType);
    typeListAdd(gen, "boden", typen[5], whichType);
    typeListAdd(gen, "gestein", typen[6], whichType);
    typeListAdd(gen, "flug", typen[7], whichType);
    typeListAdd(gen, "käfer", typen[8], whichType);
    typeListAdd(gen, "geist", typen[9], whichType);
    typeListAdd(gen, "psycho", typen[10], whichType);
    typeListAdd(gen, "unlicht", typen[11], whichType);
    typeListAdd(gen, "stahl", typen[12], whichType);
    typeListAdd(gen, "fee", typen[13], whichType);
    typeListAdd(gen, "drache", typen[14], whichType);
    typeListAdd(gen, "kampf", typen[15], whichType);
    typeListAdd(gen, "gift", typen[16], whichType);
    typeListAdd(gen, "eis", typen[17], whichType);
}

void MainWindow::on_feuer1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->feuer1, typen1[i][0], isGenChecked[i]);
    }
}

void MainWindow::on_feuer2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->feuer2, typen2[i][0], isGenChecked[i]);
    }
}

void MainWindow::on_pflanze1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->pflanze1, typen1[i][2], isGenChecked[i]);
    }
}
void MainWindow::on_pflanze2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->pflanze2, typen2[i][2], isGenChecked[i]);
    }
}
void MainWindow::on_wasser1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->wasser1, typen1[i][1], isGenChecked[i]);
    }
}
void MainWindow::on_wasser2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->wasser2, typen2[i][1], isGenChecked[i]);
    }
}
void MainWindow::on_normal1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->normal1, typen1[i][4], isGenChecked[i]);
    }
}
void MainWindow::on_normal2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->normal2, typen2[i][4], isGenChecked[i]);
    }
}
void MainWindow::on_boden1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->boden1, typen1[i][5], isGenChecked[i]);
    }
}
void MainWindow::on_boden2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->boden2, typen2[i][5], isGenChecked[i]);
    }
}
void MainWindow::on_gestein1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->gestein1, typen1[i][6], isGenChecked[i]);
    }
}
void MainWindow::on_gestein2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->gestein2, typen2[i][6], isGenChecked[i]);
    }
}
void MainWindow::on_elektro1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->elektro1, typen1[i][3], isGenChecked[i]);
    }
}
void MainWindow::on_elektro2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->elektro2, typen2[i][3], isGenChecked[i]);
    }
}
void MainWindow::on_kaefer1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->kaefer1, typen1[i][8], isGenChecked[i]);
    }
}
void MainWindow::on_kaefer2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->kaefer2, typen2[i][8], isGenChecked[i]);
    }
}
void MainWindow::on_geist1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->geist1, typen1[i][9], isGenChecked[i]);
    }
}
void MainWindow::on_geist2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->geist2, typen1[i][9], isGenChecked[i]);
    }
}
void MainWindow::on_stahl1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->stahl1, typen1[i][12], isGenChecked[i]);
    }
}
void MainWindow::on_stahl2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->stahl2, typen2[i][12], isGenChecked[i]);
    }
}
void MainWindow::on_eis1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->eis1, typen1[i][17], isGenChecked[i]);
    }
}
void MainWindow::on_eis2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->eis2, typen2[i][17], isGenChecked[i]);
    }
}
void MainWindow::on_drache1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->drache1, typen1[i][14], isGenChecked[i]);
    }
}
void MainWindow::on_drache2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->drache2, typen2[i][14], isGenChecked[i]);
    }
}
void MainWindow::on_fee1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->fee1, typen1[i][13], isGenChecked[i]);
    }
}
void MainWindow::on_fee2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->fee2, typen2[i][13], isGenChecked[i]);
    }
}
void MainWindow::on_kampf1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->kampf1, typen1[i][15], isGenChecked[i]);
    }
}
void MainWindow::on_kampf2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->kampf2, typen2[i][15], isGenChecked[i]);
    }
}
void MainWindow::on_flug1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->flug1, typen1[i][7], isGenChecked[i]);
    }
}
void MainWindow::on_flug2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->flug2, typen2[i][7], isGenChecked[i]);
    }
}
void MainWindow::on_gift1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->gift1, typen1[i][16], isGenChecked[i]);
    }
}
void MainWindow::on_gift2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->gift2, typen2[i][16], isGenChecked[i]);
    }
}
void MainWindow::on_psycho1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->psycho1, typen1[i][10], isGenChecked[i]);
    }
}
void MainWindow::on_psycho2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->psycho2, typen2[i][10], isGenChecked[i]);
    }
}
void MainWindow::on_unlicht1_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->unlicht1, typen1[i][11], isGenChecked[i]);
    }
}
void MainWindow::on_unlicht2_stateChanged()
{
    for (int i = 1; i <= 7; i++)
    {
        refreshPkmnList(this->ui->unlicht2, typen2[i][11], isGenChecked[i]);
    }
}

void MainWindow::on_removeButton_clicked()
{
    QModelIndexList selectedList = ui->listWidget->selectionModel()->selectedIndexes(); // take the list of selected indexes
    if (selectedList.isEmpty())
    {
        qDebug() << "nein junge";
        return;
    }
    int deletedID = getId(ui->listWidget->selectedItems().first()->text());
    qDebug() << "ID: " << deletedID;
    std::sort(selectedList.begin(),selectedList.end(),[](const QModelIndex& a, const QModelIndex& b)->bool{return a.row()>b.row();}); // sort from bottom to top
    for(const QModelIndex& singleIndex : selectedList)
    {
        ui->listWidget->model()->removeRow(singleIndex.row()); // remove each row
        pkmnlist.removeAll(deletedID);
        removedlist.append(deletedID);
        //qDebug() << this->ui->listWidget->item(singleIndex.row())->text().toInt()-1;
        anzahl = pkmnlist.count();
        this->ui->label_anzahl->setText(QString::number(anzahl));
        std::sort(removedlist.begin(), removedlist.end());
        refreshListWidget(this->ui->listWidget_2, removedlist);
    }

}


void MainWindow::sortlist()
{
    std::sort(pkmnlist.begin(), pkmnlist.end());
    std::sort(pkmnlist_backup.begin(), pkmnlist_backup.end());
}

void MainWindow::refreshListWidget(QListWidget* listw, QList<int> list)
{
    listw->clear();
    for (int i = 0; i < list.count(); i++)
    {
        //listw->addItem(QString::number(list.value(i)));
        listw->addItem(getName(list.value(i)));
    }
}

void MainWindow::on_restore_clicked()
{
    this->ui->listWidget->clear();
    for (int i = 0; i < pkmnlist_backup.count(); i++)
    {
        //this->ui->listWidget->addItem(QString::number(pkmnlist_backup.value(i)));
        this->ui->listWidget->addItem(getName(pkmnlist_backup.value(i)));
    }

    pkmnlist = pkmnlist_backup;
    anzahl = pkmnlist.count();
    this->ui->label_anzahl->setText(QString::number(anzahl));

    this->ui->listWidget_2->clear();
    removedlist.clear();
}

void MainWindow::on_addButton_clicked()
{
    QModelIndexList selectedList = ui->listWidget_2->selectionModel()->selectedIndexes(); // take the list of selected indexes
    if (selectedList.isEmpty())
    {
        qDebug() << "nein junge";
        return;
    }
    int deletedID = getId(ui->listWidget_2->selectedItems().first()->text());
    qDebug() << "ID: " << deletedID;
    std::sort(selectedList.begin(),selectedList.end(),[](const QModelIndex& a, const QModelIndex& b)->bool{return a.row()>b.row();}); // sort from bottom to top
    for(const QModelIndex& singleIndex : selectedList)
    {
        ui->listWidget_2->model()->removeRow(singleIndex.row()); // remove each row
        removedlist.removeAll(deletedID);
        pkmnlist.append(deletedID);
        //qDebug() << this->ui->listWidget_2->item(singleIndex.row())->text().toInt()-1;
        anzahl = pkmnlist.count();
        this->ui->label_anzahl->setText(QString::number(anzahl));
        std::sort(pkmnlist.begin(), pkmnlist.end());
        refreshListWidget(this->ui->listWidget, pkmnlist);
    }

}

void MainWindow::on_save_clicked()
{
    QString filename = this->ui->lineEdit->text();
    this->ui->lineEdit->clear();
    if (filename[0] == " " || filename == "")
    {
        qDebug() << "nein junge";
        return;
    }
    QString path = "states/" + filename + ".ptg";
    qDebug() << path;
    if (!QDir("states").exists())
    {
        QDir().mkdir("states");
    }

    QFile file (path);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "could not open file";
        return;
    }

    QTextStream out(&file);

    // anzahl der checkboxen
    out << anzCheckboxes << "\n";

    // alle checkboxen
    for (int i = 0; i < 6; i++)
    {
        foreach (QCheckBox *cb,allCheckBoxes[i])
        {
            out << cb->isChecked() << "\n";
        }
    }

    //anzahl der pokemon in liste speichern
    out << pkmnlist.count() << "\n";


    //pokemon in liste speichern
    for (int i = 0; i < pkmnlist.count(); i++)
    {
        out << pkmnlist.value(i) << "\n";
    }

    //anzahl der manuell entfernten pokemon speichern
    out << removedlist.count() << "\n";

    //manuell entfernte pokemon speichern
    for (int i = 0; i < removedlist.count(); i++)
    {
        out << removedlist.value(i) << "\n";
    }

    //anzahl der manuell entfernten pokemon speichern
    out << pkmnlist_backup.count() << "\n";


    //manuell entfernte pokemon speichern
    for (int i = 0; i < pkmnlist_backup.count(); i++)
    {
        out << pkmnlist_backup.value(i) << "\n";
    }

    if (this->ui->statesList->count() == 0)
    {
        this->ui->statesList->addItem(filename);
    }
    else
    {
        for (int i = 0; i < this->ui->statesList->count(); i++)
        {
            if (filename == this->ui->statesList->itemText(i))
            {
                    break;
            }
            this->ui->statesList->addItem(filename);
        }
    }

}

void MainWindow::on_load_clicked()
{
    if (this->ui->statesList->currentText() == "")
    {
        qDebug() << "nein junge";
        return;
    }
    QString path = "states/" + this->ui->statesList->currentText() + ".ptg";
    qDebug() << path;
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);
    while (!in.atEnd())
    {
        int anzahlCheckBoxen = in.readLine().toInt();
        int tempAnzahlCheckBoxen = 0;

        for (int i = 0; i < 6 && !in.atEnd(); i++)
        {
            foreach (QCheckBox *cb,allCheckBoxes[i])
            {
                if (in.atEnd())
                {
                    qDebug() << "file zu ende!";
                    break;
                }
                int zahl = in.readLine().toInt();
                if (zahl != 0 && zahl != 1)
                {
                    qDebug() << "Zahl weder 0 noch 1!";
                    return;
                }
                //QString line = in.readLine();
                cb->setChecked(zahl);
                tempAnzahlCheckBoxen++;
            }
        }
        if (tempAnzahlCheckBoxen != anzahlCheckBoxen)
        {
            qDebug() << "irgendwas ist hier kaputt";
            return;
        }

        int anzInList = in.readLine().toInt();

        QList<int> tempPkmnList;
        for (int i = 0; i < anzInList; i++)
        {
            tempPkmnList.append(in.readLine().toInt());
        }
        int anzRmList = in.readLine().toInt();

        QList<int> tempRemovedList;
        for (int i = 0; i < anzRmList; i++)
        {
            tempRemovedList.append(in.readLine().toInt());
        }
        int anzBackList = in.readLine().toInt();

        QList<int> tempBackupList;
        for (int i = 0; i < anzBackList; i++)
        {
            tempBackupList.append(in.readLine().toInt());
        }

        pkmnlist_backup = tempBackupList;
        pkmnlist = tempPkmnList;
        removedlist = tempRemovedList;

        refreshListWidget(this->ui->listWidget, pkmnlist);
        refreshListWidget(this->ui->listWidget_2, removedlist);

        anzahl = pkmnlist.count();
        this->ui->label_anzahl->setText(QString::number(anzahl));
    }
}

void MainWindow::on_checkAll_clicked()
{
    for (int i = 0; i < 6; i++)
    {
        foreach (QCheckBox *cb,allCheckBoxes[2])
        {
            cb->setChecked(true);
        }
    }
}

void MainWindow::on_uncheckAll_clicked()
{
    for (int i = 0; i < 6; i++)
    {
        foreach (QCheckBox *cb,allCheckBoxes[2])
        {
            cb->setChecked(false);
        }
    }
}

void MainWindow::on_checkAll_2_clicked()
{
    for (int i = 0; i < 6; i++)
    {
        foreach (QCheckBox *cb,allCheckBoxes[3])
        {
            cb->setChecked(true);
        }
    }
}

void MainWindow::on_uncheckAll_2_clicked()
{
    for (int i = 0; i < 6; i++)
    {
        foreach (QCheckBox *cb,allCheckBoxes[3])
        {
            cb->setChecked(false);
        }
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    if (this->ui->statesList->currentText() == "")
    {
        qDebug() << "nein junge";
        return;
    }
    QString filename = "states/" + this->ui->statesList->currentText() + ".ptg";
    qDebug() << QFile::remove(filename);
    this->ui->statesList->removeItem(this->ui->statesList->currentIndex());
}

void MainWindow::openPkmnInfo (QString text)
{
    QRegExp rx("(\\:)"); //RegEx for ' ' or ',' or '.' or ':' or '\t'
    QStringList query = text.split(rx);
    int id = query.first().toInt();
    pkmnInfo p(this, id);
    p.setModal(true);
    p.exec();
}

void MainWindow::on_search_textEdited(const QString &arg1)
{
    for (int i = 1; i < pkmnlist.count(); i++)
    {
            this->ui->listWidget->item(i-1)->setHidden(false);
    }
    this->ui->listWidget->item(pkmnlist.count()-1)->setHidden(false);
    for (int i = 1; i < pkmnlist.count(); i++)
    {
        if(!getName(i).startsWith(arg1, Qt::CaseInsensitive))
        {
            this->ui->listWidget->item(i-1)->setHidden(true);
        }
    }
    if(!getName(pkmnlist.count()).startsWith(arg1, Qt::CaseInsensitive))
    {
        this->ui->listWidget->item(pkmnlist.count()-1)->setHidden(true);
    }
}

void MainWindow::on_team1Widget_itemDoubleClicked(QListWidgetItem *item)
{
    openPkmnInfo(item->text());
}
void MainWindow::on_team2Widget_itemDoubleClicked(QListWidgetItem *item)
{
    openPkmnInfo(item->text());
}
void MainWindow::on_team3Widget_itemDoubleClicked(QListWidgetItem *item)
{
    openPkmnInfo(item->text());
}
void MainWindow::on_team4Widget_itemDoubleClicked(QListWidgetItem *item)
{
    openPkmnInfo(item->text());
}
void MainWindow::on_team5Widget_itemDoubleClicked(QListWidgetItem *item)
{
    openPkmnInfo(item->text());
}
void MainWindow::on_team6Widget_itemDoubleClicked(QListWidgetItem *item)
{
    openPkmnInfo(item->text());
}
void MainWindow::on_team7Widget_itemDoubleClicked(QListWidgetItem *item)
{
    openPkmnInfo(item->text());
}
void MainWindow::on_team8Widget_itemDoubleClicked(QListWidgetItem *item)
{
    openPkmnInfo(item->text());
}
